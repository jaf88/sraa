<nav class="navbar navbar-expand-sm navbar-dark bg-success p-0">
	<div class="container">
    	<a href="${linkTo[MainController].dashboard}" class="navbar-brand"><i class="fas fa-graduation-cap"> SRAA</i></a>
      	<button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
       		<span class="navbar-toggler-icon"></span>
     	</button>
    	<div class="collapse navbar-collapse" id="navbarCollapse">
       		<ul class="navbar-nav">
          	<li class="nav-item px-2">
           	 <a href="${linkTo[MainController].dashboard}" class="nav-link active"> Administração</a>
          	</li>
          	<li class="nav-item px-2">
           	 <a href="${linkTo[MainController].entenda}" class="nav-link"> Entenda ATPA/PCCS</a>
          	</li>
        </ul>

        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown mr-3">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <i class="fas fa-user"></i> 
              Bem Vindo ${(sessaoInfo.logado.descricao)}
           
            </a>
            <div class="dropdown-menu">
              <a href="profile.html" class="dropdown-item">
                <i class="fas fa-user-circle"></i> Seus Dados
              </a>
            </div>
          </li>
          <li class="nav-item">
            <a href="${linkTo[AdminController].index}" class="nav-link">
              <i class="fas fa-user-times"></i> Sair
            </a>
          </li>
        </ul>
      </div>
    </div>
</nav>