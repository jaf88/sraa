<!-- ADD Grupos / Permiss�es -->
  <div class="modal fade text-dark" id="addGrupoPermissaoModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-success text-white">
          <h5 class="modal-title">Grupos e Permiss�es de Usu�rios   <i class="fas fa-users-cog"></i></h5>
          <button class="close" data-dismiss="modal">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs nav-pills">
<!--             <br> -->
            <li class="active">
                <a class="nav-link" data-toggle="tab" href="#grupoUsuario">Grupo de Usu�rio <i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#permissaoUsuario">Permiss�o Usu�rio <i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#permissaoGrupoUsuario">Permiss�o Grupo Usu�rio <i class="fas fa-plus"></i></a>
            </li>
          </ul>
          <br>
          <div class="tab-content">
            <div id="grupoUsuario" class="tab-pane fade in active">
              <form>
                <div class="form-group">
                  <label for="name">Nome do Grupo</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="name">Menu do Grupo</label>
                  <textarea type="text" class="form-control"  placeholder="Html do Menu do Usu�rio.."></textarea>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <label><input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio"> Ativo</label>
                  <label><input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
                </div>
              </form>
            </div>
            <div id="permissaoUsuario" class="tab-pane fade">
              <form>
                <div class="form-group">
                  <label for="name">Url</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <label><input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio"> Ativo</label>
                  <label><input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
                </div>
              </form>
            </div>
            <div id="permissaoGrupoUsuario" class="tab-pane fade">
              <form>
                <div class="form-group">
                  <label for="name">Grupo de Usu�rio</label>
                  <select class="form-control" name="">
                    <option value="grupo">Grupo 1</option>
                    <option value="grupo">Grupo 2</option>
                  </select>
                </div>
                <label for="permissao">Buscar Url da Permiss�o</label>
                <div class="input-group md-form form-sm form-2 pl-0">
                  <input class="form-control my-0 py-1 red-border" type="text" placeholder="Buscar">
                  <div class="input-group-append">
                    <span class="input-group-text red lighten-3" id="basic-text1"><i class="fas fa-search text-grey" aria-hidden="true"></i></span>
                  </div>
                </div> <br>
                <div class="form-group">
                  <label>Status</label>
                  <label><input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio"> Ativo</label>
                  <label><input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning" data-dismiss="modal">Salvar</button>
        </div>
      </div>
    </div>
  </div>