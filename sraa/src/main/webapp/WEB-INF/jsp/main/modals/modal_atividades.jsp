  <!-- MODALS -->
  <!-- ADD ATIVIDADES COMPLEMENTARES MODAL -->
  <div class="modal fade text-dark" id="addAtividadeComplementaresModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-success text-white">
          <h5 class="modal-title"> Atividades complementares <i class="fas fa-pencil-alt"></i></h5>
          <button class="close" data-dismiss="modal">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs nav-pills">
<!--             <br/> -->
            <li class="active">
                <a class="nav-link" data-toggle="tab" href="#addClasseAtividade">Classe Atividade <i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#addCategoriaAtividade">Categoria Atividades <i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#addAtividades">Adicionar Atividades <i class="fas fa-plus"></i></a>
            </li>
          </ul>
          <br>
          <div class="tab-content">
            <div id="addClasseAtividade" class="tab-pane fade in active">
              <form>
                <div class="form-group">
                  <label for="nomecategoria">Nome da Classe de Atividade</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <!-- Buscar via JSON-->
                  <label for="" class="form-group">Curso</label>
                  <select class="form-control" name="">
                    <option value="">Teste1</option>
                    <option value="">Teste2</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="sigla">Sigla da Categoria</label>
                  <input type="text" class="form-control">
                </div>

                <div class="form-group">
                  <label>Status</label>
                  <label><input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio"> Ativo</label>
                  <label><input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
                </div>
              </form>
            </div>
            <div id="addCategoriaAtividade" class="tab-pane fade">
              <form>
                <div class="form-group">
                  <label for="nomecategoria">Nome da Categoria</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="sigla">Sigla da Categoria</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="" class="form-group">Tipo Classe de Atividade</label>
                  <select class="form-control" name="">
                    <option value="">Teste1</option>
                    <option value="">Teste2</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <label><input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio"> Ativo</label>
                  <label><input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
                </div>
              </form>
            </div>
            <div id="addAtividades" class="tab-pane fade">
              <form>
                <div class="form-group">
                  <label for="nomecategoria">Nome da Atividade</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="sigla">Sigla da Atividade</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="sigla">Quantidade M�xima de horas</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="" class="form-group">Categoria de Atividade</label>
                  <select class="form-control" name="">
                    <option value="">Teste1</option>
                    <option value="">Teste2</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <label><input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio"> Ativo</label>
                  <label><input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success" data-dismiss="modal">Salvar</button>
        </div>
      </div>
    </div>
  </div>