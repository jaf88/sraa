<!-- ADD Usuários -->
  <div class="modal fade text-dark" id="addUsuarioModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-success text-white">
          <h5 class="modal-title">Adicionar Usuários<i class="fas fa-users"></i></h5>
          <button class="close" data-dismiss="modal">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs nav-pills">
<!--             <br> -->
            <li class="active">
                <a class="nav-link" data-toggle="tab" href="#addProfessor">Professor<i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#addAluno">Aluno <i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#permissaoGrupoUsuario">Administrador <i class="fas fa-plus"></i></a>
            </li>
          </ul>
          <br>
          <div class="tab-content">
            <div id="addProfessor" class="tab-pane fade in active">
              <form>
                <div class="form-group">
                  <label for="name">Nome Completo</label>
                  <input type="text" class="form-control">
                </div>

                <div class="form-group">
                  <select class="form-control" name="Seleciona">
                    <option value="">Sexo</option>
                    <option value="">Masculino</option>
                    <option value="">Feminino</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="message">CPF</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label for="message">Siape</label>
                  <input type="text" class="form-control">
                </div>
                <div class="form-group">
                  <select class="form-control" name="Seleciona">
                    <option value="">Curso</option>
                    <option value="">Licenciatura em Informática</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control">
                </div>
                <div class="form-group">
                  <label for="senha">Senha</label>
                  <input type="password" class="form-control">
                </div>
                <div class="form-group">
                  <label for="confirmasenha">Confirma Senha</label>
                  <input type="password" class="form-control">
                </div>
              </form>
            </div>
            <div id="addAluno" class="tab-pane fade">
              <form>
                <div class="form-group">
                  <label for="nomeAluno">Nome Completo</label>
                  <input type="text" class="form-control">
                </div>

                <div class="form-group">
                  <select class="form-control" name="Seleciona">
                    <option value="">Sexo</option>
                    <option value="">Masculino</option>
                    <option value="">Feminino</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="message">CPF</label>
                  <input type="text" class="form-control" maxlength="11" pattern="[0-9]+$">
                </div>
                <div class="form-group">
                  <label for="telefone">Telefone</label>
                  <input type="text" maxlength="14" placeholder="XX X XXXX-XXXX" class="form-control"
                    pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$" OnKeyPress="formatar('## # ####-####', this)">
                </div>
                <div class="form-group">
                  <label for="matricula">Matricula</label>
                  <input type="text" class="form-control" maxlength="13" pattern="[0-9]+$">
                </div>
                <div class="form-group">
                  <select class="form-control" name="Seleciona">
                    <option value="">Curso</option>
                    <option value="">Licenciatura em Informática</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control">
                </div>
                <div class="form-group">
                  <label for="senha">Senha</label>
                  <input type="password" class="form-control">
                </div>
                <div class="form-group">
                  <label for="confirmasenha">Confirma Senha</label>
                  <input type="password" class="form-control">
                </div>
              </form>

            </div>
            <div id="addAdministrador" class="tab-pane fade">

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning" data-dismiss="modal">Salvar</button>
        </div>
      </div>
    </div>
  </div>