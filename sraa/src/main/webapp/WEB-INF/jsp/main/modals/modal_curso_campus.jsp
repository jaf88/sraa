<!-- ADD CAMPUS E CURSO MODAL -->
  <div class="modal fade text-dark" id="addCursoModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary text-white">
          <h5 class="modal-title">Campus <i class="fas fa-school"></i> / Curso <i class="fas fa-graduation-cap"></i></h5>
          <button class="close" data-dismiss="modal">
            <span>&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs nav-pills">
<!--             <br> -->
            <li class="active">
                <a class="nav-link" data-toggle="tab" href="#home">Campus <i class="fas fa-plus"></i></a>
            </li>
            <li>
                <a class="nav-link" data-toggle="tab" href="#menu1">Curso <i class="fas fa-plus"></i></a>
            </li>
          </ul>
          <br>
          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <%@ include file="../../campus/formulario.jsp" %>
            </div>
            <div id="menu1" class="tab-pane fade">
                <%@ include file="../../curso/formulario.jsp" %>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button  class="btn btn-danger" 
          	data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>