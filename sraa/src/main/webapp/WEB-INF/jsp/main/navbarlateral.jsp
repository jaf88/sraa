<nav
	class="navbar navbar-expand-lg flex-column navbar-light bg-info p-0 bordertop">
	<span class="navbar-toggler spancolor" data-toggle="collapse"
		data-target="#navbarNav" aria-controls="navbarNav"
		aria-expanded="false" aria-label="Alterna navega��o"> <span
		class="spancolor">Funcionalidades</span> <span
		class="navbar-toggler-icon"></span>
	</span>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav flex-column corconfig">
			<li class="nav-item"><a class="btn corconfig" role="button"
				aria-haspopup="true" aria-expanded="false"> Configura��es </a></li>
			<li class="nav-item"><a class="btn corconfig "
				href="${linkTo[CampusController].listar}"> Campus</a></li>
			<li class="nav-item"><a class="btn corconfig"
				href="${linkTo[CursoController].listar}"> Cursos</a> <br></li>
			<!--        <li class="nav-item"> -->
			<%--         <a class="btn corconfig" href="${linkTo[CursoController].listar}"> --%>
			<!--         Usuarios</a> <br>   -->
			<!--       </li> -->
			<li class="nav-item">
				<div class="dropdown-divider"></div>
			</li>
			<li class="nav-item"><a class="btn corconfig"
				href="${linkTo[AtividadeController].listar}">Atividade</a></li>
			<li class="nav-item"><a class="btn corconfig"
				href="${linkTo[ClasseAtividadeController].listar}">Classe da
					Atividade</a></li>
			<li class="nav-item"><a class="btn corconfig"
				href="${linkTo[CategoriaController].listar}">Categoria</a></li>
			<li class="nav-item">
				<div class="dropdown-divider"></div>
			</li>
			
			<li class="nav-item"><a class="btn corconfig"
			    href="${linkTo[GrupousuarioController].listar}"
				href="${linkTo[GrupousuarioController].listar}">Grupos de <br>
					Usu�rios</a></li>
			<li class="nav-item"><a class="btn corconfig"
				href="${linkTo[PermissaoController].listar}">Permiss�es</a></li>
			<li class="nav-item"><a class="btn corconfig"
				href="${linkTo[UsuarioController].listar }">Usu�rio</a></li>
		</ul>
	</div>
</nav>