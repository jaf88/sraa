<%@ include file="../geral/head.jsp" %>

	<body>
		<%@ include file="navbaradmin.jsp" %>
		  
		<!-- HEADER -->
	  <header id="main-header" class="bg-info py-2 bg-info text-white">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-6">
	          <h3>
	            <i class="fas fa-cog"></i> Administração Geral</h3>
	        </div>
	      </div>
	    </div>
	  </header>
	  
	  <div class="row" style="margin-right: 0;">
		  <div class="col-lg-2 col-md-2 col-sm-2 col-2"></div>
		  <div class="col-lg-8 col-md-8 col-sm-8 col-8">
	         <br>
	            <a href="${linkTo[MainController].dashboardalunoprofessor}" 
	               class="btn btn-outline-info btn-lg btn-block">
	            Gerenciar / Alunos / Professores<i class="fas fa-cog"></i> <br><br>
	            </a>
	           <br><br>
	           <a href="${linkTo[MainController].dashboardadmin}" class="btn btn-outline-info btn-lg btn-block">
	           Configurações Gerais<i class="fas fa-cog"></i> <br><br>  
	           </a>
	        </div>
	        <div class="col-lg-2 col-md-2 col-sm-2 col-2"></div>
        </div>
<!-- 	   Footer  Rodape -->
        <%@ include file="../geral/footer.jsp" %>
        
        <!--  Scripts -->
    	<%@ include file="../geral/scripts.jsp" %>
	</body>
</html>

