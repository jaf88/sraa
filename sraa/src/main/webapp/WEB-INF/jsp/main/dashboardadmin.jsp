<%@ include file="../geral/head.jsp" %>

    <body>
        <%@ include file="navbaradmin.jsp" %>
        
        
      <header id="main-header" class="bg-info py-2 bg-info text-white">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h3>
                <i class="fas fa-cog"></i> Administra��o Geral</h3>
            </div>
          </div>
        </div>
      </header>
      
      <div class="row" style="margin-right: 0;">
        <div class="col-lg-2 col-md-2 col-sm-2 col-12">
<!--        <br> -->
          <%@ include file="navbarlateral.jsp" %>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-2"></div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-5">
            <div class="container">
                <br> <br> <br>
                <div class="card">Seja bem vindo as configura��es gerais!<br> 
                    D�vidas, acesse ao manual do administrador no link abaixo! <br>
                    <a href="#">Manul do Administrador - SRAA</a></div>
            </div>
        </div>
    </div>
              
<!--       Footer  Rodape -->
        <%@ include file="../geral/footer.jsp" %>
        
         Scripts
        <%@ include file="../geral/scripts.jsp" %>
    </body>
</html>