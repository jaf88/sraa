
<!-- entrar SECTION -->
<%@ include file="../admin/modals/usuario.jsp"%>
<%@ include file="../admin/modals/aluno.jsp"%>
<%@ include file="../admin/modals/professor.jsp"%>
<header id="entrar-section">
	<div class="dark-overlay">
		<div class="entrar-inner container">
			<div class="row">
				<div class="col-lg-12 col-md-10 col-sm-10 col-12">
					<br> <br>
					<h3 class="text-center">Seja bem vindo ao Sistema de Registro
						de Atividades Acad�micas do IFPI</h3>
				</div>
			</div>
			<h4>
				<span class="alert-success">${msgSucesso}</span> <span
					class="alert-danger">${msgErro}</span>
			</h4>
			<div class="row d-flex py-1">
				<div class="col-lg-3 col-md-3 col-sm-3 col-0"></div>
				<div class="cardEntrar col-lg-6 col-md-6 col-sm-6 col-12">
					<div class="card text-dark">
						<div class="card-header">
							<ul class="nav nav-tabs card-header-tabs pull-right" id="myTab"
								role="tablist">
								<li class="nav-item"><a class="nav-link active"
									id="home-tab" data-toggle="tab" href="#home" role="tab"
									aria-controls="home" aria-selected="true">Entrar</a></li>
								<li class="nav-item"><a class="nav-link" id="profile-tab"
									data-toggle="tab" href="#profile" role="tab"
									aria-controls="profile" aria-selected="false">Cadastre-se</a></li>
								<li class="nav-item"><a class="nav-link" id="contact-tab"
									data-toggle="tab" href="#contact" role="tab"
									aria-controls="contact" aria-selected="false">Recuperar
										Senha</a></li>
							</ul>
						</div>
						<div class="card-body">
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="home" role="tabpanel"
									aria-labelledby="home-tab">
									<a href="" class="btn btn-block btn-primary"
										data-toggle="modal" data-target="#usuariomodal">
										Administra��o</a> <a href="" class="btn btn-block btn-primary"
										data-toggle="modal" data-target="#professormodal">
										Professor</a> <a href="" data-toggle="modal"
										data-target="#alunomodal" class="btn btn-block btn-primary">
										Aluno</a>
								</div>
								<div class="tab-pane fade" id="profile" role="tabpanel"
									aria-labelledby="profile-tab">
									<h5>Fa�a o seu cadastro e come�e a registrar e fazer o
										acompanhamento de suas atividades acad�micas.</h5>
									<br> <a href="${linkTo[ProfessorController].formulario}"
										class="btn btn-block btn-primary"> Professor</a> <br> <a
										href="${linkTo[AlunoController].formulario}"
										class="btn btn-block btn-primary"> Aluno</a>
								</div>
								<div class="tab-pane fade" id="contact" role="tabpanel"
									aria-labelledby="contact-tab">
									<form>
										Enviar Email de Recupera��o.
										<div class="form-group">
											<input type="email" class="form-control form-control-lg"
												placeholder="Email">
										</div>
										<a href="#" class="btn btn-primary btn-block">Enviar</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-0"></div>
			</div>
		</div>
	</div>
</header>
