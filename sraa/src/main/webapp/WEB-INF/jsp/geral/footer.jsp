  <footer id="main-footer" class="bg-white text-dark">
    <div class="container">
      <div class="row">
        <hr class="bg-dark">
        <div class="col-lg-6 col-md-6 col-sm-12 col-12"> <br>
          <img src="<c:url value="/resources/img/logoifpi.png"/> 
            "class="user-image" align="left" width="194" alt="Logo Ifpi"/>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
          <p class="lead text-center"> <br>
            <strong>SRAA - IFPI- Campus Teresina Zona Sul
             - Avenida Pedro Freitas, 1020 - S�o Pedro. Copyright &copy; <span id="year"></span></strong>
          </p>
        </div>
      </div>
    </div>
  </footer>