  <!-- ATPA HEAD -->
  <section id="atpa-head-section" class="bg-success">
    <div class="container">
      <div class="row">
        <div class="col text-center py-5">
          <br>
          <h1 class="display-5">ATPA</h1>
          <p class="lead">
              Atividades Te�rico-Pr�ticas de Aprofundamento (ATPA) em �reas
              Espec�ficas de Interesse do Estudante dos Cursos de Licenciatura do Instituto
              Federal de Educa��o, Ci�ncia e Tecnologia do Piau� - IFPI.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- ATPA SECTION -->
  <section id="atpa-section" class="bg-light py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <br>
          <div class="d-flex">
             <h5 class="display-5"> As ATPA foram regulamentadas atrav�s da resolu��o N� 017/2015 -
              Conselho Superior (CONSUP) do IFPI.
              A resolu��o encontra-se na integra no seguinte
<!--               Resolu��o hospedada no meu google drive, pois o link oficial do ifpi mudou -->
              <a class="btn btn-btn-secondary" target="_blank" 
              href="https://drive.google.com/file/d/1MlQV0C1-SPx6hy7IDldMl5ZF6_LrgzSn/view?usp=sharing">
                <strong>arquivo <i class="fas fa-file-pdf"></i> </strong>
             </a> 
<!--               <a class="btn btn-btn-secondary" target="_blank" href="https://www5.ifpi.edu.br/consup/attachments/article/16/Resolu%C3%A7%C3%A3o%20n%C2%BA%20017%20Atividades%20Te%C3%B3rico-Pr%C3%A1ticas%20de%20Aprofundamento.pdf"> -->
<!--                 <strong>arquivo <i class="fas fa-file-pdf"></i> </strong> -->
<!--               </a> -->
            </h5>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg- 12 col-md-12 col-sm-12 col-12">
          <br> <br>
          <h5 class="display-5"> <strong>Entendemos por ATPA as seguites atividades</strong></h5>
           
              <h5><i class="far fa-arrow-alt-circle-right"> 
              	Atividades de Ensino e Inicia��o � Doc�ncia.</i></h5>
              <h5><i class="far fa-arrow-alt-circle-right"> 
              	Atividades de Pesquisa.</i> </h5>
              <h5><i class="far fa-arrow-alt-circle-right"> 
              	Atividades Outras (Esportivas, Culturais, Filantr�picas, 
              	Visitas T�cnicas).</i> </h5>
           
        </div>
      </div>
    </div>
  </section>