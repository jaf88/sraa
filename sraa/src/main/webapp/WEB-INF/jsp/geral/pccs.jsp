  <!-- PCCS HEAD -->
  <section id="pccs-head-section" class="bg-success">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center py-5">
          <br>
          <h1 class="display-5">PCCS</h1>
          <p class="lead">
            Pr�ticas Curriculares em Comunidade e em Sociedade -
            nos Curr�culos dos Cursos de Gradua��o do IFPI.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- PCCS SECTION -->
  <section id="pccs-section" class="bg-light py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <br>
        <div class="d-flex">
            <h5 class="display-5">
              As PCCS foram regulamentadas atrav�s da resolu��o N� 016/2015 -
              Conselho Superior (CONSUP) do IFPI.
              A resolu��o encontra-se na integra no seguinte

              <a target="_blank" 
                href="https://drive.google.com/file/d/1OpzAF5s9k5Wp-rn8_pd-SXOyFmNwyQlZ/view?usp=sharing">
                arquivo <i class="fas fa-file-pdf"></i>
              </a>
              
<!--               <a target="_blank" href="https://www5.ifpi.edu.br/consup/attachments/article/16/Resolu%C3%A7%C3%A3o%20n%C2%BA%20016%20Regulamento%20Registro%20e%20Inclus%C3%A3o%20das%20atividades%20de%20extens%C3%A3o%20REVISADA.pdf"> -->
<!--                 arquivo <i class="fas fa-file-pdf"></i> -->
<!--               </a> -->
            </h5>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg- 12 col-md-12 col-sm-12 col-12">
            <br><br>
          <h5 class="display-5"> 
          <strong> Entendemos por PCCS as seguites 
          	atividades de extens�o</strong></h5>
          
              <h5>  <i class="far fa-arrow-alt-circle-right"> Projeto. </i></h5>
              <h5>  <i class="far fa-arrow-alt-circle-right"> Programa.</i> </h5>
              <h5>  <i class="far fa-arrow-alt-circle-right"> Curso.</i> </h5>
              <h5>  <i class="far fa-arrow-alt-circle-right"> Evento.</i></h5>
           
        </div>
      </div>
    </div>
  </section>