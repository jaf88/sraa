<%@ include file="../geral/head.jsp"%>

<body>
	<%@ include file="../main/navbaradmin.jsp"%>

	<header id="main-header" class="py-2 bg-info text-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h5>
						<a class="stretched-link text-light"
							href="${linkTo[GrupousuarioController].listar}">Lista de
							Grupos de Usu�rios </a><i class="fas fa-users"></i>
					</h5>
				</div>
				<div class="col-md-4">
					<a href="${linkTo[GrupousuarioController].formulario}"
						class="text-white">
						<button class="btn btn-warning">
							<strong>Cadastrar Novo Grupos <i class="fas fa-plus"></i></strong>
						</button>
					</a> <br>
				</div>
				<div class="col-md-4">
					<form action="${linkTo[GrupousuarioController].listar}"
						method="post" class="form-inline my-2 my-lg-0">
						<input class="form-control mr-sm-2" type="search"
							placeholder="Pesquisa Grupo" aria-label="Search"
							name="grupousuario.descricao" id="grupousuario.descricao"
							value="${grupousuario.descricao}">
						<button class="btn btn-outline-light my-2 my-sm-0" type="submit">Pesquisar</button>
					</form>
				</div>

			</div>
		</div>
	</header>

	<div class="row" style="margin-right: 0;">
		<div class="col-lg-2">
			<%@ include file="../main/navbarlateral.jsp"%>
		</div>
		<div class="col-lg-10">
			<div class="container">
				<h3>
					<span class="alert-success">${msgSucesso}</span>
				</h3>
				<h3>
					<span class="alert-danger">${msgErro}</span>
				</h3>
				<table class="table table-striped table-sm text-center">

					<tr>

						<th scope="col">Grupo de Usu�rio</th>
						<th scope="col">Adicionar Permiss�o</th>
						<th scope="col">A��es</th>
					</tr>

					<tbody class="table-striped">

						<c:forEach items="${lista}" var="grupousuario">
							<tr>
								<td>${grupousuario.descricao}</td>
								<td><a
									href="${linkTo[GrupousuarioController].detalhes}?grupousuario.id=${grupousuario.id}"
									class="btn btn-success">Adicionar</a></td>

								<td><a data-toggle="tooltip" data-placement="top"
									title="Editar"
									href="${linkTo[GrupousuarioController].editar}?grupousuario.id=${grupousuario.id}">
										<i class="far fa-edit"></i>
								</a> <a data-toggle="tooltip" data-placement="top" title="Excluir"
									href="${linkTo[GrupousuarioController].remover}?grupousuario.id=${grupousuario.id}">
										<i class="far fa-trash-alt"></i>
								</a></td>
							</tr>
						</c:forEach>
					</tbody>

				</table>
			</div>
		</div>
	</div>

	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>