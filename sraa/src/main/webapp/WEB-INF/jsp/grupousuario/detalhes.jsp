<%@ include file="../geral/head.jsp"%>

<body>
	<%@ include file="../main/navbaradmin.jsp"%>
	<div class="row">
		<div class="col-lg-2">
			<%@ include file="../main/navbarlateral.jsp"%>
		</div>
		<div class="container"><br>
		<div class="col-lg-10">
			<div class="card">
	      <div class="card-header cardcor"> <h4> Grupo de Usuarios - ${grupousuario.descricao}</h4>  </div> 
	      <div class="card-body">
	      
				<form role="form" method="post" action="${linkTo[GrupousuarioController].salvarPermissao}">
				
					<input name="grupousuario.id" type="hidden" 
						value="${grupousuario.id != null ? grupousuario.id : 0}">

			<div class="col-md-6">
				<label>Permissao</label> <select name="permissao.id"
					id="permissao.id" class="form-control">
<%-- 					<option value="0" ${grupousuario.permissoes.id == null ? 'selected' : ''}></option> --%>
					<c:forEach var="item" items="${listaPermissao}">
						<option value="${item.id}"
							${permissao.id == item.id ? 'selected' : ''}>
							${item.descricao}</option>
					</c:forEach>
				</select> <br>
				<button type="submit" class="btn btn-primary">Salvar</button>
				
			</div>
		</form>
	      </div>
	
	    	<div class="card-header cardcor"><h5>Permissoes do Grupo</h5></div>
	      		<div class="card-body">
	      		<table class="table table-striped table-sm text-center">
					
						<tr>
<!-- 							<th scope="col">Id</th> -->
							<th scope="col">Permissao</th>
							<th scope="col">Acoes</th>
						</tr>
						
	      		
	      		<tbody class="table-striped">
	      	
	      			<c:forEach items="${grupousuario.permissoes}" var="permissao">
	      				<tr>
<!-- 	      					<td></td> -->
	      					<td>${permissao.descricao}</td>
	      					
								<td>
<!-- 								<a data-toggle="tooltip" data-placement="top" title="Editar" -->
<%-- 									href="${linkTo[GrupousuarioController].editar}?grupousuario.id=${grupousuario.id}"> --%>
<!-- 										<i class="far fa-edit"></i> -->
<!-- 								</a> -->
								<a data-toggle="tooltip" data-placement="top" title="Excluir"
									href="${linkTo[GrupousuarioController].removerPermissao}?permissao.id=${permissao.id}&grupousuario.id=${grupousuario.id}">
										<i class="far fa-trash-alt"></i>
								</a></td>
	      					</tr>
	      				</c:forEach>
	      			</tbody>
	      		</table>
	     
	      </div>
	    </div>
	     <br><br>
	     <div class="text-center">
					<a class="btn btn-outline-info"
						href="${linkTo[GrupousuarioController].listar}"><i
						class="fas fa-arrow-left"></i> Voltar</a>
				</div>
	    </div>
	    </div>
	    </div>
	   
	   
	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>

<script type="text/javascript">
	$(function() {
		$("#permissaoDesc").autocomplete({
			minLength : 0,
			source : "${linkTo[JsonController].jsonData}?classe=Permissao",
			minLength : 2,
			focus : function(event, ui) {
				$("#permissaoDesc").val(ui.item.value);
				return false;
			},
			select : function(event, ui) {
				$("#permissaoDesc").val(ui.item.value);
				$("#permissaoID").val(ui.item.id);
				return false;
			}
		});

	});
</script>
</html>