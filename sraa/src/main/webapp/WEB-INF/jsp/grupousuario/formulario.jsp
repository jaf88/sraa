<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    
    <!--    <div class="container"> -->
    <div class="row">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>  
        </div>
        <div class="container">
            <div class="col-lg-10">
                <br>
                <div class="card">
                <div class="card-header cardcor">
                <h3>Cadastrar Grupo de Usu�rio</h3>
                </div>
                    <div class="card-body">
                       
					<h3><span class="alert-danger">${msgErro}</span></h3>
					
					<form action="${linkTo[GrupousuarioController].salvar}" method="post">
					<input type="hidden" name="grupousuario.id" 
					   value="${grupousuario.id==null? 0 : grupousuario.id}">
				
					  <div class="form-group">
					    <label >Descri��o:</label>
					    <input type="text" class="form-control" name="grupousuario.descricao" placeholder="Nome do grupo de usu�rio" value="${grupousuario.descricao}">
					  </div>
					  
					  <div class="form-group">
					    <label >Menu:</label>
					    <textarea class="form-control" name="grupousuario.menu" required cols="70" rows="10" placeholder="Html do Menu do Usu�rio..">${grupousuario.menu}</textarea>
					  </div>	  
					  
					  <div class="form-group">
						 <label for="nameField ">Status:</label>
							 	<label><input name="grupousuario.status" id="optionsRadios1" value="1" ${grupousuario.status == 1 || grupousuario==null ? 'checked' : ''} type="radio" > Ativo</label>
				             	<label><input name="grupousuario.status" id="optionsRadios2" value="0" ${grupousuario.status == 0 ? 'checked' : ''} type="radio"> Inativo</label>
				        </div>
					  
					  
					  <button type="submit" class="btn btn-primary">Salvar</button>
					  <button type="button" class="btn btn-primary" onClick="location='javascript:history.go(-1)'">Voltar</button>
					  
				 </form>
					                       
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[CampusController].listar}"><i
                        class="fas fa-arrow-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>