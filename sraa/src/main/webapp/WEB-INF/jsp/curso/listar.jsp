<%@ include file="../geral/head.jsp"%>

<body>
<%@ include file="../main/navbaradmin.jsp" %>
    <header id="main-header" class="py-2 bg-info text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h5>
                        <a class="stretched-link text-light"
                            href="${linkTo[CursoController].listar}">
                            Cursos Cadastrados </a> <i class="fas fa-users"></i>
                    </h5>
                </div>
                <div class="col-md-4">
                    <a href="${linkTo[CursoController].formulario }"
                        class="text-white"><button class="btn btn-warning"><strong>Cadastrar Curso <i
                        class="fas fa-plus"></i></strong></button>
                    </a> <br>
                </div>
                <div class="col-md-4">
                    <form action="${linkTo[CursoController].listar}"
                        method="post" class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search"
                            placeholder="Pesquisa Curso" aria-label="Search"
                            name="curso.descricao" id="curso.descricao"
                            value="${curso.descricao}">
                        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Pesquisar</button>
                    </form>
                </div>

            </div>
        </div>
    </header>     
       <div class="row" style="margin-right: 0;">
        <div class="col-lg-2 col-md-2 col-sm-2 col-12">
<!--        <br> -->
          <%@ include file="../main/navbarlateral.jsp" %>
        </div>

        <div class="col-lg-10 col-md-10 col-sm-10 col-12">
            <div class="container">
            <br><br>
                <div class="" role="alert">
                    <div class="container bg-light">
					   <h3><span class="alert-success">${msgSucesso}</span></h3>
					   <h3><span class="alert-danger">${msgErro}</span></h3>
					    <table class="table table-striped">
					        <thead class="thead-dark">
					            <tr>
					              <th scope="col">#</th>
					              <th scope="col">Curso</th>					        
					              <th scope="col">Status</th>
					              <th scope="col">Funcoes</th>
<!-- 					              <th scope="col">Excluir</th> -->
					            </tr>
					          </thead>
					          <tbody class="table-striped">
					          
					            <c:forEach items="${lista}" var="curso">
					            
					            <tr>
					                <td>${curso.id}</td>
					                <td>${curso.descricao}</td>					     
					                <td>${curso.status == 1 ? "Ativo" : "Inativo"}</td>
					                <td>
					                   <a data-toggle="tooltip" data-placement="top" title="Editar" 
					                       href="${linkTo[CursoController].editar}?curso.id=${curso.id}">
					                       <i class="far fa-edit"></i></a>
					                   <a data-toggle="tooltip" data-placement="top" title="Excluir"
					                       href="${linkTo[CursoController].remover}?curso.id=${curso.id}">
					                       <i class="far fa-trash-alt"></i></a></td>
					            </tr>
					            </c:forEach>  
					          </tbody>
					        
					    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--       Footer  Rodape -->
    <%@ include file="../geral/footer.jsp"%>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>