<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    
    <!--    <div class="container"> -->
    <div class="row">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>  
        </div>
        <div class="container">
            <div class="col-lg-10">
                <br>
                <div class="card">
                <div class="card-header cardcor">
                <h3>Cadastrar Curso</h3>
                </div>
                    <div class="card-body">
                       
					<h3><span class="alert-danger">${msgErro}</span></h3>

					<form action="${linkTo[CursoController].salvar}" method="post">
					    <input type="hidden" name="curso.id" value="${curso.id==null? 0 : curso.id}">
					  	<div class="form-group">
					        <label>Nome do Curso</label>
					        <input type="text" class="form-control" name="curso.descricao" value="${curso.descricao}">
					    </div>
					    
					    <div class="form-group">
					        <label>Campus</label>
					        <select name="curso.campus.id" id="curso.campus.id" class="form-control">
					            <option value="0" ${curso.campus.id == null ? 'selected' : ''} ></option>
					            <c:forEach var="item" items="${listaCampus}">
					                <option value="${item.id}" ${curso.campus.id == item.id ? 'selected' : ''} > 
					                    ${item.descricao}</option>
					            </c:forEach> 
					        </select>
					    </div>
					    
					    <div class="form-group">
					        <label for="nameField ">Status:</label>
					        <label>
					            <input name="curso.status" id="optionsRadios1" value="1" ${curso.status == 1 || curso==null ? 'checked' : ''} type="radio" >Ativo</label>
					        <label>
					            <input name="curso.status" id="optionsRadios2" value="0" ${curso.status == 0 ? 'checked' : ''} type="radio">Inativo</label>
					    </div>
					    
					     <button type="submit" class="btn btn-primary">Salvar</button>
					   
					</form>
					                       
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[CampusController].listar}"><i
                        class="fas fa-arrow-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>


