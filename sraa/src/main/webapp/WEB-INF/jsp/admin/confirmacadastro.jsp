<%@ include file="../geral/head.jsp"%>
<body data-spy="scroll" data-target="#main-nav" id="entrar">
	<nav class="navbar navbar-expand-sm bg-success navbar-dark fixed-top"
		id="main-nav">
		<div class="container">
			<a href="${linkTo[AdminController].index}" class="navbar-brand"><i
				class="fas fa-graduation-cap"> SRAA </i></a>
			<button class="navbar-toggler" data-toggle="collapse"
				data-target="#navbarCollapse">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-auto">

					<li class="nav-item"><a href="#" class="nav-link btn"
						data-toggle="modal" data-target="#contactModal"> Contato <i
							class="fas fa-comment-dots"></i></a></li>
				</ul>
			</div>
		</div>
	</nav>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class=" container">
		<h4>
			<span class="alert-success">${msgSucesso}</span>
			<%--       		<span class="alert-danger">${msgErro}</span></h4> --%>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="card col-md-6">
					<div class="card-header cardcor">
						<strong>Entrar</strong>
					</div>
					<div class="card-body">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="home" role="tabpanel"
								aria-labelledby="home-tab">
								<form action="${linkTo[AdminController].efetuaLoginAluno}"
									method="post">
									<div class="form-group">
										<input type="text" name="aluno.matricula"
											class="form-control form-control-lg" placeholder="Matricula">
									</div>
									<div class="form-group">
										<input type="password" name="aluno.senha"
											class="form-control form-control-lg" placeholder="Senha">
									</div>

									<button type="submit" class="btn btn-primary btn-block">Entrar</button>
								</form>
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-3"></div>
			</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<%@ include file="../geral/footer.jsp"%>

	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>