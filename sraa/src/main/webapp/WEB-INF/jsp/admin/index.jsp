<%@ include file="../geral/head.jsp" %>
<%@ include file="navbarindex.jsp" %>

    <body data-spy="scroll" data-target="#main-nav" id="entrar">
    <h3>
        <span class="alert-success">${msgSucesso}</span>
    </h3>
        <!-- entrar SECTION -->
        <%@ include file="../geral/login.jsp" %>
        
        <!-- atpa SECTION -->
        <%@ include file="../geral/atpa.jsp" %>
        
        <!-- pccs SECTION -->
        <%@ include file="../geral/pccs.jsp" %>
        
        <!-- Modal contato -->
        <%@ include file="../geral/modalcontact.jsp" %>
           <!-- Footer - Rodape -->
         <%@ include file="../geral/footer.jsp" %>
        
        <!--  Scripts -->
        <%@ include file="../geral/scripts.jsp" %>
    </body>
</html>