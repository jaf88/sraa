<!-- Login Professor -->
<div class="modal fade text-dark" id="professormodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login Professor</h5>
                <button class="close" data-dismiss="modal">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="${linkTo[AdminController].efetuaLoginProfessorGeral}"
                    method="post">
                    <div class="form-group">
                        <strong>CPF</strong>
                        <input type="text" name="professor.cpf"
                            class="form-control form-control-lg" placeholder="CPF"
                            maxlength="11" pattern="[0-9]+$">
                    </div>
                    <div class="form-group">
                        <strong>Senha</strong>
                        <input type="password" name="professor.senha"
                            class="form-control form-control-lg" placeholder="Senha">
                    </div>

                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </form>
            </div>
        </div>
    </div>
</div>