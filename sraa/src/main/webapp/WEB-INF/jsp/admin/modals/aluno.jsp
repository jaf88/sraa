<!-- Login aluno -->
<div class="modal fade text-dark" id="alunomodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Login Aluno</h5>
				<button class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="${linkTo[AdminController].efetuaLoginAlunoGeral}"
					method="post">
					<div class="form-group">
					    <strong>Matricula</strong>
						<input type="text" name="aluno.matricula"
							class="form-control form-control-lg" placeholder="Matricula">
					</div>
					<div class="form-group">
				        <strong>Senha</strong>
						<input type="password" name="aluno.senha"
							class="form-control form-control-lg" placeholder="Senha">
					</div>

					<button type="submit" class="btn btn-primary btn-block">Entrar</button>
				</form>
			</div>
		</div>
	</div>
</div>