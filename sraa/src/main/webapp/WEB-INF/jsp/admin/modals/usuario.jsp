<!-- Login administrativo -->
<div class="modal fade text-dark" id="usuariomodal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Login Administrativo</h5>
				<button class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="${linkTo[AdminController].efetuaLogin}" method="post">
					<div class="form-group">
						<input type="email" name="usuario.login"
							class="form-control form-control-lg" placeholder="Email">
					</div>
					<div class="form-group">
						<input type="password" name="usuario.senha"
							class="form-control form-control-lg" placeholder="Senha">
					</div>

					<button type="submit" class="btn btn-primary btn-block">Entrar</button>
				</form>
			</div>
		</div>
	</div>
</div>