<nav class="navbar navbar-expand-sm bg-success navbar-dark fixed-top" id="main-nav">
    <div class="container">
      <a href="${linkTo[AdminController].index}" class="navbar-brand"><i class="fas fa-graduation-cap"> SRAA </i></a>
      <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a href="${linkTo[AdminController].index}" class="nav-link btn">
              Entrar <i class="fas fa-sign-in-alt"></i></a>
          </li>
<!--           <li class="nav-item"> -->
<!--             <a href="#atpa-head-section" class="nav-link btn"> -->
<!--               ATPA <i class="far fa-bookmark"></i></a> -->
<!--           </li> -->
<!--           <li class="nav-item"> -->
<!--             <a href="#pccs-head-section" class="nav-link btn"> -->
<!--               PCCS <i class="far fa-bookmark"></i></a> -->
<!--           </li> -->
      
          <li class="nav-item">
            <a href="#" class="nav-link btn" data-toggle="modal" data-target="#contactModal">
              Contato <i class="fas fa-comment-dots"></i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>