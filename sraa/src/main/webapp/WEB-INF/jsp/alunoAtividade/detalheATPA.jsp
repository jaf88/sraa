<%@ include file="../geral/head.jsp"%>
<body>
	<%@ include file="../aluno/navbar.jsp"%>

	<br>
	<br>

	<div class="container">
		<!--        <div class="col-lg-2 col-md-3 col-sm-2 col-1"></div> -->
		<div class="card">
			<div class="card-header bg-success">
				<div class="row">
					<div class="col-md-8">
						<h5>
							<a class="stretched-link text-light" href="#">Resumo da ATPA
								Cadastrada <i class="fas fa-clipboard-check"></i>
							</a>
						</h5>
					</div>
					<div class="col-md-4"></div>
				</div>

			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-8 col-8">
						<strong>Atividade Cadastrada:</strong> ${alunoAtividade.descricao}
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-4">
						<strong>Carga Hor�ria:</strong> ${alunoAtividade.cargaHoraria} Hrs
					</div>
				</div>
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-8 col-8">
						<strong>Tipo de Atividade:</strong>
						${alunoAtividade.atividade.descricao}
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-4">
						<c:choose>
							<c:when test="${alunoAtividade.status == 0}">
								<strong>Situa��o: </strong>
								<a href="#" class="btn btn-warning btn-sm">Em Analise</a>
							</c:when>
							<c:when test="${alunoAtividade.status == 1 }">
								<strong>Situa��o: </strong>>	
								    <a href="#" class="btn btn-warning btn-sm">
								    Indeferido</a>
							</c:when>
							<c:otherwise>
								<strong>Situa��o: </strong>
								<a href="#" class="btn btn-success btn-sm">Deferido</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<div class="row">
				    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
				    <hr>
				        <strong>Observa��es: </strong>
				        ${alunoAtividade.observacoes}        
				    </div>
				</div>
			</div>
		</div>

	</div>

	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>
