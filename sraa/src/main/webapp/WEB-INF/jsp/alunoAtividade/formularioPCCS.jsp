<%@ include file="../geral/head.jsp"%>
<%@ include file="../aluno/navbar.jsp"%>

<body data-spy="scroll" data-target="#main-nav" id="entrar">
    <br>
	<br>
	<br>
	<div class="container">
		<div class="card">
			<div class="card-header bg-success">
				<h3>Cadastrar PCCS</h3>
			</div>
			<div class="card-body">
				<form action="${linkTo[AlunoAtividadeController].salvar}"  method="post">
				 <input type="hidden" name="alunoAtividade.id"
			        value="${alunoAtividade.id==null? 0 : alunoAtividade.id}">
			     <input type="hidden" name="alunoAtividade.aluno.id"
			        value="${sessaoInfo.logaluno.id}">  
			     <input type="hidden" name="alunoAtividade.status"
			        value=0>  
<!-- 			     <input type="hidden" name="alunoAtividade.professor.id" -->
<!-- 			        value=0>  -->
					<div class="form-group">
				    <label for="name"> <strong>Descri��o da Atividade</strong></label> 
				    <input type="text" name="alunoAtividade.descricao"
				        value="${alunoAtividade.descricao}"class="form-control">
				     </div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="message"><strong>Periodo da Atividade</strong></label> 
							<input type="text" name="alunoAtividade.periodo" 
							 value="${alunoAtividade.periodo}"class="form-control">
						</div>
						<div class="form-group col-md-4">
                            <label for="message"><strong>Carga Horaria</strong></label> <input type="text"
                                name="alunoAtividade.cargaHoraria" value="${alunoAtividade.cargaHoraria}"class="form-control" >
                        </div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label><strong>Tipo de PCCS</strong></label> 
							<select name="alunoAtividade.atividade.id"
								id="alunoAtividade.atividade.id" class="form-control">
								<option value="0" ${alunoAtividade.atividade.id == null ? 'selected' : ''}>
								Selecione uma PCCS</option>
								<c:forEach var="item" items="${listaAtividade}">
									<option value="${item.id}"
										${alunoAtividade.atividade.id == item.id ? 'selected' : ''}>
										${item.descricao}</option>
								</c:forEach>
							</select>
						</div>
						
					</div>
					<div class="form-row">
					
						<div class="col-md-3 text-rigth">
							<br />
							<button type="submit" class="btn btn-primary">
								<strong>Cadastrar</strong>
							</button>
							<a href="${linkTo[AlunoController].pccsdashboard}" class="btn btn-danger">
								Cancelar </a>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>