
<%-- <%@ include file="/template/menu_lateral.jsp" %> --%>


<div class="container">
	<div class="jumbotron">
		<h1>Detalhes Categoria Cadastrada</h1>
		
		<h3><span class="alert-success">${msgSucesso}</span></h3>
		
		<div class="form-group">
			<label>Nome Categoria</label> ${categoria.descricao}
		</div>
		
		
		<div class="form-group">
			<label>Sigla da Categoria:</label> ${categoria.sigla}
		</div>
		
		<div class="form-group">
			<label>Tipo de Atividade</label> ${categoria.classeAtividade.descricao}
		</div>
	</div>
</div>
