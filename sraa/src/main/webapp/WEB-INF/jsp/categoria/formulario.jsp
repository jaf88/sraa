<%-- <h3><span class="alert-danger">${msgErro}</span></h3> --%>
<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    
    <!--    <div class="container"> -->
    <div class="row">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>  
        </div>
        <div class="container">
            <div class="col-lg-10">
                <br>
                <div class="card">
                <div class="card-header cardcor">
                <h3>Cadastro de Categorias</h3>
                </div>
                    <div class="card-body">
                       
					<h3><span class="alert-danger">${msgErro}</span></h3>
					<form class="form-horizontal" role="form" method="post" 
					action="${linkTo[CategoriaController].salvar}">
	            	<input type="hidden" name="categoria.id" 
	            		value="${categoria.id==null? 0 : categoria.id}">
	            	<div class="box-body">
	               		<div class="form-group">
		                  	<label class="col-sm-4 control-label">Nome Categoria</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="Nome do Classe Atividade" 
	    							name="categoria.descricao" value="${categoria.descricao}">
		                  	</div>
	                	</div>
	                
	                <div class="form-group">
		                  	<label class="col-sm-4 control-label">Sigla da Categoria</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="Sigla da Classe Atividade" 
	    							name="categoria.sigla" value="${categoria.sigla}">
		                  	</div>
	               	</div>
	               	
	              	<div class="form-group">
	                	<label class="col-sm-4 control-label">Tipo de Atividade</label>
	                	<div class="col-sm-8">
	                    	<select name="categoria.classeAtividade.id" id="categoria.classeAtividade.id" class="form-control">
	        	 			<option value="0" ${categoria.classeAtividade.id == null ? 'selected' : ''} ></option>
		                	<c:forEach var="item" items="${listaClasseAtividade}"> 
		                		<option value="${item.id}" ${categoria.classeAtividade.id == item.id ? 'selected' : ''} >${item.descricao}</option>
		            		</c:forEach> 
	        				</select>
	                  	</div>
	                </div>
		 			
		 			<div class="form-group">
				     	<label class="col-sm-4 control-label">Status:</label>
				    	<div class="col-sm-8" > 
					 	<label><input name="categoria.status" id="optionsRadios1" value="1" ${categoria.status == 1 || categoria==null ? 'checked' : ''} type="radio" >Ativo</label>
		             	<label><input name="categoria.status" id="optionsRadios2" value="0" ${categoria.status == 0 ? 'checked' : ''} type="radio">Inativo</label>
		   		    </div>
		        </div>
			</div>
	              <div class="box-footer">
	              <button type="submit" class="btn btn-info pull-center">Salvar</button>
<%-- 	                <a href="${linkTo[MainController].home}" class="btn btn-info pull-rigthr">Salvar</a> --%>
	              </div>
	            </form>                       
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[CategoriaController].listar}"><i
                        class="fas fa-arrow-left"></i>Voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>