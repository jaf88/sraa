<%@ include file="../geral/head.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>

	<br>
	<br>

	<div class="container">
<!-- 		<div class="col-lg-2 col-md-3 col-sm-2 col-1"></div> -->
		<div class="card">
			<div class="card-header bg-success">
			     <div class="row">
				    <div class="col-md-8">
						<h5>
							<a class="stretched-link text-light"
							href="#">Pr�tica Curricular em 
								Comunidade e em Sociedade <i class="fas fa-clipboard-check"></i></a>
						</h5>
				</div>
				<div class="col-md-4">
			     	<a href="${linkTo[AlunoAtividadeController].formularioPCCS}"
						class="text-white"> <button class="btn btn-primary"><strong>Cadastrar PCCS <i
						class="fas fa-plus"></i></strong></button>
					</a>
					<a href="#"
                       class="text-white"> <button class="btn btn-primary"><strong>Relatorio 
                       <i class="fas fa-file-alt"></i></strong></button>
                        </a>
					<br>
				</div>

			</div>
		</div>

			</div>
			<div class="card-body">
				<%@ include file="pccs/listarPCCS.jsp" %>
			</div>
		</div>
		<div class="col-lg-2 col-md-3 col-sm-2 col-1"></div>
	</div>
	
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>
