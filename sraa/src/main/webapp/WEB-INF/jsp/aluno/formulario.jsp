<%@ include file="../geral/head.jsp"%>
<%@ include file="../admin/navadminaux.jsp"%>

<body data-spy="scroll" data-target="#main-nav" id="entrar">
    <br>
	<br>
	<br>
	<div class="container">
		<div class="card">
			<div class="card-header bg-success">
				<h3>Cadastro de Alunos</h3>
			</div>
			<div class="card-body">
				<form action="${linkTo[AlunoController].salvar}"  method="post">
				 <input type="hidden" name="aluno.id"
			        value="${aluno.id==null? 0 : aluno.id}">
			     <input type="hidden" name="aluno.grupousuario.id"
			        value=3>  
					<div class="form-group">
				    <label for="name"> <strong>Nome Completo</strong></label> 
				    <input type="text" name="aluno.nome"
				        value="${aluno.nome}"class="form-control">
				     </div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label><strong>Sexo</strong></label> 
                       <select class="form-control"name="aluno.sexo" id="aluno.sexo">
                          <option value="">Selecione</option>
                          <option value="${aluno.sexo}Masculino">Masculino</option>
                          <option value="${aluno.sexo}Feminino">Feminino</option>
                       </select>
						</div>
						<div class="form-group col-md-4">
							<label><strong>CPF</strong></label> <input type="text"
								name="aluno.cpf" value="${aluno.cpf}"class="form-control" 
								maxlength="11" pattern="[0-9]+$">
						</div>
						<div class="form-group col-md-4">
							<label for="matricula"><strong>Matricula</strong></label> <input type="text"
								name="aluno.matricula" value="${aluno.matricula}" 
								class="form-control" maxlength="13" pattern="[0-9]+$">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="telefone"><strong>Telefone</strong></label> <input type="text"
								maxlength="14" placeholder="XX X XXXX-XXXX" 
								name="aluno.telefone" value="${aluno.telefone}"class="form-control"
								pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$"
								OnKeyPress="formatar('## # ####-####', this)">
						</div>
						<div class="form-group col-md-4">
							<label><strong>Curso</strong></label> <select name="aluno.curso.id"
								id="aluno.curso.id" class="form-control">
								<option value="0" ${aluno.curso.id == null ? 'selected' : ''}></option>
								<c:forEach var="item" items="${listaCurso}">
									<option value="${item.id}"
										${aluno.curso.id == item.id ? 'selected' : ''}>
										${item.descricao}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-md-4">
							<label for="email"><strong>Email</strong></label> <input type="email"
								name="aluno.email" value="${aluno.email}"class="form-control">
						</div>
					</div>
					<div class="form-row">
				        <div class="form-group col-md-3">
				            <label for="ano"><strong>Ano de Ingresso</strong></label> 
				            <input type="text" name="aluno.anoIngresso" 
				                value="${aluno.anoIngresso}"
				                class="form-control">
				        </div>
						<div class="form-group col-md-3">
							<label for="senha"><strong>Senha</strong></label> <input type="password"
								name="aluno.senha" value="${aluno.senha}"class="form-control">
						</div>
						<div class="form-group col-md-3">
							<label for="confirmasenha"><strong>Confirma Senha</strong></label> <input
								name="aluno.confsenha" value="${aluno.senhaConf}"
								type="password" class="form-control">
						</div>
						<div class="col-md-3 text-rigth">
							<br />
							<button type="submit" class="btn btn-primary">
								<strong>Cadastrar</strong>
							</button>
							<a href="${linkTo[AdminController].index}" class="btn btn-danger">
								Cancelar </a>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
	<!-- Modal contato -->
	<%@ include file="../geral/modalcontact.jsp"%>
	<!-- Footer - Rodape -->
	<%@ include file="../geral/footer.jsp"%>

	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>