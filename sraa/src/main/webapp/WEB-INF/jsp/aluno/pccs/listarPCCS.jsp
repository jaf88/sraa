
<table class="table table-responsive table-striped table-sm text-center">
	<tr>
		<th class="bg-dark text-white" scope="col">Atividade Casdastrada</th>
		<th class="bg-dark text-white" scope="col">Carga Horaria</th>
		<th class="bg-dark text-white" scope="col">Tipo de Atividade</th>
		<th class="bg-dark text-white" scope="col">Situa��o</th>
		<th class="bg-dark text-white" scope="col">Op��es</th>
	</tr>
	<tbody class="table-striped">
		<c:forEach items="${listaAtividade}" var="atividade">
			<c:if test="${atividade.aluno.id == sessaoInfo.logaluno.id }">
				<tr>
					<td>${atividade.descricao}</td>
					<td>${atividade.cargaHoraria}Hrs</td>
					<td>${atividade.atividade.descricao}</td>
					<td><c:choose>
							<c:when test="${atividade.status == 0}">
								<a
									href="${linkTo[AlunoAtividadeController].detalhePCCS}?alunoAtividade.id=${atividade.id}"
									class="btn btn-warning">Em Analise</a>
							</c:when>
							<c:when test="${atividade.status == 1 }">
								<a
									href="${linkTo[AlunoAtividadeController].detalhePCCS}?alunoAtividade.id=${atividade.id}"
									class="btn btn-warning">Indeferido</a>
							</c:when>
							<c:otherwise>
								<a
									href="${linkTo[AlunoAtividadeController].detalhePCCS}?alunoAtividade.id=${atividade.id}"
									class="btn btn-success">Deferido</a>
							</c:otherwise>
						</c:choose></td>
					<td><a data-toggle="tooltip" data-placement="right"
						title="Editar" href="#"><i class="far fa-edit"></i></a> <a
						data-toggle="tooltip" data-placement="right" title="Excluir"
						href="#"><i class="far fa-trash-alt"></i></a></td>

				</tr>
			</c:if>
		</c:forEach>
	</tbody>

</table>