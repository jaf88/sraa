<%-- <h3><span class="alert-danger">${msgErro}</span></h3> --%>
<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    
    <!--    <div class="container"> -->
    <div class="row">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>  
        </div>
        <div class="container">
            <div class="col-lg-10">
                <br>
                <div class="card">
                <div class="card-header cardcor">
                <h3>Cadastrar Classe de Atividades</h3>
                </div>
                    <div class="card-body">
                       
					<h3><span class="alert-danger">${msgErro}</span></h3>
					
					<form class="form-horizontal" role="form" method="post" action="${linkTo[ClasseAtividadeController].salvar}">
	            	<input type="hidden" name="classeAtividade.id" value="${classeAtividade.id==null? 0 : classeAtividade.id}">
	            	<div class="box-body">
	               		<div class="form-group">
		                  	<label class="col-sm-4 control-label">Nome da Classe de Atividade</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="Nome do Classe Atividade" 
	    							name="classeAtividade.descricao" value="${classeAtividade.descricao}">
		                  	</div>
	                	</div>
	                
	                <div class="form-group">
		                  	<label class="col-sm-4 control-label">Sigla da Atividade</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="Sigla da Classe Atividade" 
	    							name="classeAtividade.sigla" value="${classeAtividade.sigla}">
		                  	</div>
	               	</div>
		 			
		 			<div class="form-group">
				     	<label class="col-sm-4 control-label">Status:</label>
				    	<div class="col-sm-8" > 
					 	<label><input name="classeAtividade.status" id="optionsRadios1" value="1" ${classeAtividade.status == 1 || classeAtividade==null ? 'checked' : ''} type="radio" >Ativo</label>
		             	<label><input name="classeAtividade.status" id="optionsRadios2" value="0" ${classeAtividade.status == 0 ? 'checked' : ''} type="radio">Inativo</label>
		   		    </div>
		        </div>
			</div>
	              <div class="box-footer">
	              <button type="submit" class="btn btn-info pull-center">Salvar</button>
<%-- 	                <a href="${linkTo[MainController].home}" class="btn btn-info pull-rigthr">Salvar</a> --%>
	              </div>
	            </form>
					                       
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[ClasseAtividadeController].listar}"><i
                        class="fas fa-arrow-left"></i>Voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>