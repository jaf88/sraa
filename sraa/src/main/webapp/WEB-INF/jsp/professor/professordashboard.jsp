<%@ include file="../geral/head.jsp"%>
<body>
	<%@ include file="navbar.jsp"%>
	<header id="main-header" class="py-2 bg-success text-white">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h3>
						<i class="fas fa-cog"></i> Painel de Controle Professor
					</h3>
				</div>
			</div>
		</div>
	</header>
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2 col-2"></div>
			<div class="col-lg-8 col-md-8 col-sm-8 col-8">
				<br> <a href="#" class="btn btn-outline-info btn-lg btn-block">ATPA
					- Avaliar<i class="fas fa-cog"></i> <br>
				<br> <!--             <div class="progress"  style="height: 50px;">  -->
					<!--                 <div class="progress-bar progress-bar-striped  bg-danger" role="progressbar" style="width: 50%;"  -->
					<!--                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div> <br> <br> -->
					<!--            </div> -->
					<button></button>
				</a> <br>
				<br> <a href="#" class="btn btn-outline-info btn-lg btn-block">PCCS
					- Avaliar<i class="fas fa-cog"></i> <br>
				<br> <!--            <div class="progress" style="height: 50px;"> -->
					<!--                 <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: 40%;"  -->
					<!--                     aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">40%</div> -->
					<!--            </div> -->
				</a>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-2"></div>
		</div>
	</div>

	<!--       Footer  Rodape -->
	<br>
	<br>
	<br>
	<br>
	<br>
	<%@ include file="../geral/footer.jsp"%>

	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>

