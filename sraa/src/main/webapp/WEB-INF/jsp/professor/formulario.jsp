<%@ include file="../geral/head.jsp" %>
<%@ include file="../admin/navadminaux.jsp" %>

<body data-spy="scroll" data-target="#main-nav" id="entrar">
<br> <br> <br>
<h3>
        <span class="alert-success">${msgSucesso}</span>
    </h3>
    <h3>
        <span class="alert-success">${msgErro}</span>
    </h3>
    <div class="container">
        <div class="card">
            <div class="card-header bg-success">		
                <h3>Cadastro de Professores</h3>
            </div>
            <div class="card-body">
                <form action="${linkTo[ProfessorController].salvar}" method="post">
			        <input type="hidden" name="professor.id"
			              value="${professor.id==null? 0 : professor.id}">
			        <input type="hidden" name="professor.grupousuario.id"
                        value=2> <!-- Observa��o, esse grupo � setado via BD com valor igual a 2 -->
				    <div class="form-row">
				     <div class="form-group col-lg-7 col-md-7 col-sm-7 col-7">
					    <label for="name"> <strong>Nome Completo</strong></label> 
					    <input type="text" name="professor.nome"
					        value="${professor.nome}"class="form-control">
				     </div>
		          <div class="form-group col-lg-5 col-md-5 col-sm-5 col-5">
                        <label><strong>Email</strong></label> <input type="email"
                             name="professor.email" value="${professor.email}"class="form-control">                 
                  </div>
			         
                    </div>
                    
			     <div class="form-row">
			     <div class="form-group col-lg-3 col-md-3 col-sm-3 col-3">
                        <label><strong>CPF</strong></label> 
                        <input type="text" name="professor.cpf"
                              value="${professor.cpf}" maxlength="11" pattern="[0-9]+$" 
                              class="form-control">
                     </div>
				    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-3">
					   <label><strong>Sexo</strong></label> 
					   <select class="form-control"name="professor.sexo" id="professor.sexo">
						  <option value="">Selecione</option>
						  <option value="${professor.sexo}Masculino">Masculino</option>
						  <option value="${professor.sexo}Feminino">Feminino</option>
					   </select>
				    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-3">
                        <label><strong>Telefone</strong></label> <input type="text"
	                        name="professor.telefone" value="${professor.telefone}"
	                        maxlength="14" placeholder="XX X XXXX-XXXX" class="form-control"
	                        pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$"
	                        OnKeyPress="formatar('## # ####-####', this)">
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-3">
	                    <label><strong>Curso</strong></label> 
	                    <select name="professor.curso.id"
	                              id="professor.curso.id" class="form-control">
	                       <option value="0" ${professor.curso.id == null ? 'selected' : ''}></option>
	                       <c:forEach var="item" items="${listaCurso}">
	                        <option value="${item.id}"
	                            ${professor.curso.id == item.id ? 'selected' : ''}>
	                            ${item.descricao}</option>
	                       </c:forEach>
	                    </select>
                    </div>

			     </div>
			 <div class="form-row">
				<div class="form-group col-md-4">
					
				</div>
			     <div class="form-group col-md-4">
				    
			     </div>
			    <div class="form-group col-md-4">
                    
                </div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-3">
					<label><strong>Senha</strong></label> <input type="password"
						name="professor.senha" value="${professor.senha}" class="form-control">
				</div>
				<div class="form-group col-md-3">
					<label><strong>Confirma Senha</strong></label> <input
						type="password" class="form-control">
				</div>
				<div class="col-md-3 text-rigth"></div>
				<div class="col-md-3 text-rigth">
				
				<br/>

					<button type="submit" class="btn btn-primary">
						<strong>Cadastrar</strong>
					</button>
					<a href="${linkTo[AdminController].index}"
								class="btn btn-danger"> Cancelar </a>
				</div>
			</div>
		</form>
	</div>
    </div>
</div>

        <!-- Modal contato -->
        <%@ include file="../geral/modalcontact.jsp" %>
           <!-- Footer - Rodape -->
         <%@ include file="../geral/footer.jsp" %>
        
        <!--  Scripts -->
        <%@ include file="../geral/scripts.jsp" %>
    </body>
</html>
		<!--         </div> -->
		<!--         <div class="modal-footer"> -->
		<!--           <a class="btn btn-primary" href="dashboardprofessor.html">Enviar</a> -->
		<!--         </div> -->
		<!--       </div> -->
		<!--     </div> -->
		<!--   </div> -->