<div class="container">
	<div class="jumbotron">
		<h1>Detalhes Atividade Cadastrada</h1>
		
		<h3><span class="alert-success">${msgSucesso}</span></h3>
		
		<div class="form-group">
			<label>Id</label> ${atividade.id}
		</div>
		
		<div class="form-group">
			<label>Nome Atividade</label> ${atividade.descricao}
		</div>
		
		
		<div class="form-group">
			<label>Sigla da Atividade:</label> ${atividade.sigla}
		</div>
		
		<div class="form-group">
			<label>M�ximo:</label> ${atividade.maximo}
		</div>
		
		<div class="form-group">
			<label>Tipo de Atividade</label> ${atividade.categoria.descricao}
		</div>
	</div>
</div>
