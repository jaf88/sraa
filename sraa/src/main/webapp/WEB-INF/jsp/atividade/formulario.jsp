<%-- <h3><span class="alert-danger">${msgErro}</span></h3> --%>
<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    
    <!--    <div class="container"> -->
    <div class="row">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>  
        </div>
        <div class="container">
            <div class="col-lg-10">
                <br>
                <div class="card">
                <div class="card-header cardcor">
                <h3>Cadastro de Atividade</h3>
                </div>
                    <div class="card-body">
                       
					<h3><span class="alert-danger">${msgErro}</span></h3>
					<form class="form-horizontal" action="${linkTo[AtividadeController].salvar}" 
					role="form" method="post">
	            	<input type="hidden" name="atividade.id" 
	            		value="${atividade.id==null? 0 : atividade.id}">
	            	<div class="box-body">
	               		<div class="form-group">
		                  	<label class="col-sm-4 control-label">Nome Atividade</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="Nome da Atividade" 
	    							name="atividade.descricao" value="${atividade.descricao}">
		                  	</div>
	                	</div>
	                
	                <div class="form-group">
		                  	<label class="col-sm-4 control-label">Sigla da Atividade</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="Sigla da Atividade" 
	    							name="atividade.sigla" value="${atividade.sigla}">
		                  	</div>
	               	</div>
	               	
	               	 <div class="form-group">
		                  	<label class="col-sm-4 control-label">M�ximo</label>
		    	            <div class="col-sm-8">
	    						<input type="text" class="form-control" placeholder="M�ximo" 
	    							name="atividade.maximo" value="${atividade.maximo}">
		                  	</div>
	               	</div>
	               	
	              	<div class="form-group">
	                	<label class="col-sm-4 control-label">Categoria da Atividade</label>
	                	<div class="col-sm-8">
	                    	<select name="atividade.categoria.id" id="atividade.categoria.id" class="form-control">
	        	 			<option value="0" ${atividade.categoria.id == null ? 'selected' : ''} ></option>
		                	<c:forEach var="item" items="${listaCategoria}"> 
		                		<option value="${item.id}" ${atividade.categoria.id == item.id ? 'selected' : ''} >${item.descricao}</option>
		            		</c:forEach> 
	        				</select>
	                  	</div>
	                </div>
		 			
		 			<div class="form-group">
				     	<label class="col-sm-4 control-label">Status:</label>
				    	<div class="col-sm-8" > 
					 	<label><input name="atividade.status" id="optionsRadios1" value="1" ${atividade.status == 1 || atividade==null ? 'checked' : ''} type="radio" >Ativo</label>
		             	<label><input name="atividade.status" id="optionsRadios2" value="0" ${atividade.status == 0 ? 'checked' : ''} type="radio">Inativo</label>
		   		    </div>
		        </div>
			</div>
	              <div class="box-footer">
	              <button type="submit" class="btn btn-info pull-center">Salvar</button>
<%-- 	                <a href="${linkTo[MainController].home}" class="btn btn-info pull-rigthr">Salvar</a> --%>
	              </div>
	            </form> 
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[AtividadeController].listar}"><i
                        class="fas fa-arrow-left"></i>Voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>