
<%-- <h3><span class="alert-danger">${msgErro}</span></h3> --%>
<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
<!--         <div class="container"> -->
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-2">
            <%@ include file="../main/navbarlateral.jsp"%>
        </div>


            <div class="col-lg-10 col-md-10 col-sm-10 col-10">
                <br>
                <div class="card">
                    <div class="card-header cardcor">
                        <h3>Cadastrar Nova Permiss�o</h3>
                    </div>
                    <div class="card-body">
                       <form action="${linkTo[PermissaoController].salvar}" method="post">
                    <input type="hidden" name="permissao.id"
                        value="${permissao.id==null? 0 : permissao.id}">
                    <div class="form-group">
                        <label for="name"><strong>Url</strong></label> <input type="text"
                            class="form-control" name="permissao.descricao"
                            value="${permissao.descricao}">
                    </div>
                    <div class="form-group">
                        <label><strong>Status:</strong></label> <label><input
                            name="permissao.status" id="optionsRadios1" value="1"
                            ${permissao.status == 1 || permissao==null ? 'checked' : ''}
                            type="radio"> Ativo</label> <label><input
                            name="permissao.status" id="optionsRadios2" value="0"
                            ${permissao.status == 0 ? 'checked' : ''} type="radio">
                            Inativo</label>
                    </div>
                    <div class="text-right">
                    <button id="botaosalvar" type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[PermissaoController].listar}"><i
                        class="fas fa-arrow-left"></i> Voltar</a>
                </div>
            </div>
        </div>
<!--     </div> -->
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>