<%@ include file="../geral/head.jsp"%>

<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    <header id="main-header" class="py-2 bg-info text-white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h5>
                        <a class="stretched-link text-light"
                            href="${linkTo[PermissaoController].listar}">Lista de
                            Permiss�es Cadastradas</a> <i class="fas fa-users"></i>
                    </h5>
                </div>
                <div class="col-md-4">
                    <a href="${linkTo[PermissaoController].formulario }"
                        class="text-white"><button class="btn btn-warning"><strong> Cadastrar Nova Permiss�o <i
                        class="fas fa-plus"></i></strong></button>
                    </a> <br>
                </div>
                <div class="col-md-4">
                    <form action="${linkTo[PermissaoController].listar}" method="post"
                        class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search"
                            placeholder="Pesquisa Permiss�o" aria-label="Search"
                            name="grupousuario.descricao" id="grupousuario.descricao"
                            value="${permissao.descricao}">
                        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Pesquisar</button>
                    </form>
                </div>

            </div>
        </div>
    </header>

    <div class="row" style=" margin-right: 0;">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>
        </div>
        <div class="col-lg-10">
            <div class="container">

                <h3>
                    <span class="alert-success">${msgSucesso}</span>
                </h3>
                <h3>
                    <span class="alert-danger">${msgErro}</span>
                </h3>
                <table class="table table-striped table-sm">
                    
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Url</th>
                            <th scope="col">Status</th>
                            <th scope="col">A��es</th>
                        </tr>
                    
                    <tbody class="table-striped">

                        <c:forEach items="${lista}" var="permissao">

                            <tr>
                                <td>${permissao.id}</td>
                                <td>${permissao.descricao}</td>
                                <td>${permissao.status == 1 ? "Ativo":"Inativo"}</td>
                                <td><a data-toggle="tooltip" data-placement="top" title="Editar"
                                    href="${linkTo[PermissaoController].editar}?permissao.id=${permissao.id}">
                                        <i class="far fa-edit"></i>
                                </a>
                                <a data-toggle="tooltip" data-placement="top" title="Excluir"
                                    href="${linkTo[PermissaoController].remover}?permissao.id=${permissao.id}">
                                        <i class="far fa-trash-alt"></i>
                                </a></td>
                            </tr>
                        </c:forEach>
                    </tbody>

                </table>

            </div>
        </div>
    </div>





    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>