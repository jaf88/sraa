<%@ include file="../geral/head.jsp"%>
<body>
	<%@ include file="../main/navbaradmin.jsp"%>

	<!-- 	<div class="container"> -->
	<div class="row">
		<div class="col-lg-2">
			<%@ include file="../main/navbarlateral.jsp"%>
		</div>
		<div class="container">
			<div class="col-lg-10">
				<br>
				<div class="card">
					<div class="card-header cardcor">
						<h3>Cadastrar Novo Usu�rio</h3>
					</div>
					<div class="card-body">
					<form action="${linkTo[UsuarioController].salvar}" method="post">
					    <input type="hidden" name="usuario.id"
					        value="${usuario.id==null? 0 : usuario.id}">
					    <div class="form-group">
					        <label for="name"><strong>Nome do Servidor</strong></label> <input type="text"
					            class="form-control" placeholder="Nome do Servidor"
					            name="usuario.descricao" value="${usuario.descricao}">
					    </div>
					
					    <div class="form-group">
					        <label for="name"><strong>Email</strong></label> <input type="text"
					            class="form-control" placeholder="Digite um email"
					            name="usuario.login" value="${usuario.login}">
					    </div>
					    <div class="form-row">
					        <div class="form-group col-md-6">
					            <label for="name"><strong>Senha</strong></label> <input type="password"
					                class="form-control" placeholder="Senha" name="usuario.senha"
					                value="${usuario.senha}">
					        </div>
					        <div class="form-group col-md-6">
					            <label for="name"><strong>Confirmar Senha</strong></label> <input type="password"
					                class="form-control" placeholder="Confirmar Senha"
					                name="usuario.senhaConf" value="${usuario.senhaConf}">
					        </div>
					    </div>
					    <div class="form-group">
					        <label><strong>Grupo de Usu�rio</strong></label> <select name="usuario.grupousuario.id"
					            id="usuario.grupousuario.id" class="form-control">
					            <option value="0"
					                ${usuario.grupousuario.id == null ? 'selected' : ''}></option>
					            <c:forEach var="item" items="${listaGrupoUsuario}">
					                <option value="${item.id}"
					                    ${usuario.grupousuario.id == item.id ? 'selected' : ''}>
					                    ${item.descricao}</option>
					            </c:forEach>
					        </select>
					    </div>
					
					    <div class="form-group">
					        <label for="nameField"><strong>Status:</strong></label> <label><input
					            name="usuario.status" id="optionsRadios1" value="1"
					            ${usuario.status == 1 || usuario==null ? 'checked' : ''} type="radio">
					            Ativo</label> <label><input name="usuario.status" id="optionsRadios2"
					            value="0" ${usuario.status == 0 ? 'checked' : ''} type="radio">
					            Inativo</label>
					    </div>
					    <div class="text-right">
					    <button id="botaosalvar" type="submit" class="btn btn-primary">Salvar</button>
					    </div>
					</form>
					</div>
				</div>
				<br>
				<div class="text-center">
					<a class="btn btn-outline-info"
						href="${linkTo[UsuarioController].listar}"><i
						class="fas fa-arrow-left"></i> Voltar</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 	</div> -->
	<%@ include file="../geral/scripts.jsp"%>
</body>