<%@ include file="../geral/head.jsp"%>
<body>
	<%@ include file="../main/navbaradmin.jsp"%>


	<header id="main-header" class="py-2 bg-info text-white">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h5>
						<a class="stretched-link text-light"
							href="${linkTo[UsuarioController].listar }"> Lista dos
							Usu�rios </a><i class="fas fa-users"></i>
					</h5>
				</div>
				<div class="col-md-4">
					<a href="${linkTo[UsuarioController].formulario }"
						class="text-white"><button class="btn btn-warning"><strong>Cadastrar Novo Usu�rio <i
						class="fas fa-plus"></i></strong> </button>
					</a> <br>
				</div>
				<div class="col-md-4">
					<form action="${linkTo[UsuarioController].listar}" method="post"
						class="form-inline my-2 my-lg-0">
						<input class="form-control mr-sm-2" type="search"
							placeholder="Pesquisa Usu�rio" aria-label="Search"
							name="usuario.descricao" id="usuario.nome"
							value="${usuario.nome}">
						<button class="btn btn-outline-light my-2 my-sm-0" type="submit">Pesquisar</button>
					</form>
				</div>

			</div>
		</div>
	</header>

	<div class="row" style="margin-right: 0;">
		<div class="col-lg-2">
			<%@ include file="../main/navbarlateral.jsp"%>
		</div>

		<div class="col-lg-10">
			<div class="container">
				<h3>
					<span class="alert-success">${msgSucesso}</span>
				</h3>
				<h3>
					<span class="alert-danger">${msgErro}</span>
				</h3>
				<table class="table table-striped table-sm"  id="cabecalho">
					 <thead class="thead-dark">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Usu�rio</th>
							<th scope="col">Grupo Usu�rio
							<th scope="col">Status</th>
							<th scope="col">A��es</th>
						</tr>
					</thead>
					<tbody class="table-striped">

						<c:forEach items="${lista}" var="usuario">

							<tr>
								<td>${usuario.id}</td>
								<td>${usuario.nome}</td>
								<td>${usuario.grupousuario.descricao}</td>
								<td>${usuario.status == 1? "Ativo" : "Inativo"}</td>
								<td><a data-toggle="tooltip" data-placement="right" title="Editar"
									href="${linkTo[UsuarioController].editar}?usuario.id=${usuario.id}">
										<i class="far fa-edit"></i>
								</a>
								<a data-toggle="tooltip" data-placement="top" title="Excluir"
									href="${linkTo[UsuarioController].remover}?usuario.id=${usuario.id}">
										<i class="far fa-trash-alt"></i>
								</a></td>
							</tr>
						</c:forEach>
					</tbody>

				</table>
			</div>
		</div>
	</div>

	<!--  Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>
