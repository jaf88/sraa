<%@ include file="../geral/head.jsp"%>
<body>
	<%@ include file="../main/navbaradmin.jsp"%>

	<div class="row">
		<div class=col-lg-2>
			<%@ include file="../main/navbarlateral.jsp"%>
		</div>
		<div class="container"><br>
		<div class=col-lg-10>
				<div class="card">
						<div class="card-header">
						<h3>Editar Usu�rio</h3>
						</div>
						<br><h3 class="text-center">OBS: Por favor, insira novamente sua senha e confirme!</h3>
						<div class="card-body">
						<%@include file="formulario.jsp"%>
					</div>
					
				</div><br>
				<div class="text-center">
					<a class="btn btn-outline-info"
						href="${linkTo[UsuarioController].listar}"><i
						class="fas fa-arrow-left"></i> Voltar</a>
				</div>
			</div>
		</div>
	</div>



	<!--      Scripts -->
	<%@ include file="../geral/scripts.jsp"%>
</body>
</html>
