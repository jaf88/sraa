<%@ include file="../geral/head.jsp"%>
<body>
    <%@ include file="../main/navbaradmin.jsp"%>
    
    <!--    <div class="container"> -->
    <div class="row">
        <div class="col-lg-2">
            <%@ include file="../main/navbarlateral.jsp"%>  
        </div>
        <div class="container">
            <div class="col-lg-10">
                <br>
                <div class="card">
                <div class="card-header cardcor">
                <h3>Cadastrar Campus</h3>
                </div>
                    <div class="card-body">
                       
					<h3><span class="alert-danger">${msgErro}</span></h3>
					
					<form action="${linkTo[CampusController].salvar}" method="post">
					    <input type="hidden" name="campus.id" 
					        value="${campus.id==null? 0 : campus.id}">
					    <div class="form-group">
					        <label for="name">Nome do Campus</label>
					        <input type="text" class="form-control"
					            placeholder="Nome do Campus" name="campus.descricao" 
					            value="${campus.descricao}">
					    </div>
					    <div class="form-group">
					        <label for="name">Endere�o do Campus</label>
					        <input type="text" class="form-control"
					            placeholder="Endere�o do Campus" name="campus.endereco" 
					            value="${campus.endereco}">
					    </div>
					    <div class="form-group">
					        <label>Status</label>
					        <label>
					            <input name="campus.status" id="optionsRadios1" 
					                value="1" ${campus.status == 1 || campus==null ? 'checked' : ''} 
					                type="radio"> Ativo</label>
					        <label>
					            <input name="campus.status" id="optionsRadios2" 
					                 value="0" ${campus.status == 0 ? 'checked' : ''} 
					                 type="radio"> Inativo</label>
					     </div>
					      <button type="submit" class="btn btn-success">Salvar</button>
					</form>
					                       
                    </div>
                </div>
                <br>
                <div class="text-center">
                    <a class="btn btn-outline-info"
                        href="${linkTo[CampusController].listar}"><i
                        class="fas fa-arrow-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>
    <!--  Scripts -->
    <%@ include file="../geral/scripts.jsp"%>
</body>
</html>
