package br.com.sraa.sistema.controller;


import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.bo.CategoriaBO;
import br.com.sraa.sistema.bo.ClasseAtividadeBO;
import br.com.sraa.sistema.model.Categoria;
import br.com.sraa.sistema.model.ClasseAtividade;


@Controller
public class CategoriaController extends DefaultController  {
	
	private CategoriaBO bo;
	
	public CategoriaController(){}
	
	@Inject
	public CategoriaController(Result result,  Dao dao) {
		super(result,dao);
		bo = new CategoriaBO(dao);
	}
	 
	@Override
	public void formulario(){
		try {
			result.include("listaClasseAtividade",new ClasseAtividadeBO(dao).lista(new ClasseAtividade()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	
	public void listar(Categoria categoria) { 
		 try {
			this.result.include("categoria",categoria);
			lista =  bo.lista(categoria);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	public void salvar(Categoria categoria) {
		try {
			categoria = bo.armazena(categoria);
			this.result.include("categoria",categoria);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(this).listar(new Categoria());
//			this.result.redirectTo(CategoriaController.class).detalhes(categoria);
		} catch (Exception e) {
			this.result.include("categoria",categoria);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(CategoriaController.class).formulario();
		}
	}
	
	public void editar(Categoria categoria) {
		try {
			categoria = bo.procura(categoria);
			result.include("categoria",categoria);
			result.include("listaClasseAtividade",new ClasseAtividadeBO(dao).lista(new ClasseAtividade()));
		} catch (Exception e) {
			result.include("categoria",categoria);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Categoria());
		}
	}
	
	public void detalhes(Categoria categoria) {
		try {
			categoria = bo.procura(categoria);
			result.include("categoria",categoria);
		} catch (Exception e) {
			result.include("categoria",categoria);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Categoria());
		}
//		this.editar(categoria);
	}
	
	public void remover(Categoria categoria) {
		try {
			bo.remove(categoria);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(CategoriaController.class).listar(new Categoria());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	
}