package br.com.sraa.sistema.bo;


import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.model.Curso;


public class CursoBO {
	
	private Dao dao;
	
	public CursoBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Curso procura(Curso dto) {
		 Curso aux = (Curso) dao.procura(dto.getId(), Curso.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Curso armazena (Curso dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Descrição deve ser informado!");
		/* if (dto.getDatanasc() == null) 
				throw new Exception("O campo Data de Nascimento deve ser informado!"); */		 
		 // colocar aqui as outras valida�oes
		 return (Curso)dao.salva(dto);
	}


	 public List<Object> lista(Curso dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Curso.class);
		ct.createAlias("campus", "campus");

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("descricao",dto.getDescricao()).ignoreCase());
		
//		 if (dto.getCampus()!=null &&  dto.getCampus().getDescricao()!=null &&  !dto.getCampus().getDescricao().equals("")) 
//				ct.add(Restrictions.like("campus.descricao",dto.getCampus().getDescricao(),MatchMode.ANYWHERE).ignoreCase());
//					
		return ct.list();
	 }
	 
	 public void remove(Curso dto) throws Exception {
			dao.remove(procura(dto));
		}
	
}
