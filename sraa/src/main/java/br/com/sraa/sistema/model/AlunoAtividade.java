
package br.com.sraa.sistema.model;

import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table (name="alunoatividade")
public class AlunoAtividade {

	private Long id;
	private Aluno aluno;
	private Professor professor;
	private Atividade atividade;
	private String descricao;
	private String periodo;
	private String observacoes;
	private Integer cargaHoraria;
	private Date datacriacao;
	private Date datamodificacao;
	private Integer Status;
	
	
	
//	private Set<Professor> avaliarprofessor = new HashSet<Professor>(0);
	
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "idaluno", nullable = false)
	public Aluno getAluno() {
		return aluno;
	}
	
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "idatividade", nullable = false)
	public Atividade getAtividade() {
		return atividade;
	}
	public void setAtividade(Atividade atividade) {
		this.atividade = atividade;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}
	public Integer getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(Integer cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public Date getDatacriacao() {
		return datacriacao;
	}
	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}
	
	public Date getDatamodificacao() {
		return datamodificacao;
	}
	public void setDatamodificacao(Date datamodificacao) {
		this.datamodificacao = datamodificacao;
	}
	public Integer getStatus() {
		return Status;
	}
	public void setStatus(Integer status) {
		Status = status;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	
//	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
//	@JoinTable(name = "avaliaratividaprofessor",
//	joinColumns = @JoinColumn(name = "idalunoatividade", referencedColumnName = "id"),
//	inverseJoinColumns = @JoinColumn(name = "idprofessor", referencedColumnName = "id"))
//	public Set<Professor> getAvaliarprofessor() {
//		return avaliarprofessor;
//	}
//
//	public void setAvaliarprofessor(Set<Professor> avaliarprofessor) {
//		this.avaliarprofessor = avaliarprofessor;
//	}
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "idprofessor", nullable = false)
	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
}
