package br.com.sraa.sistema.bo;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.model.Campus;


public class CampusBO {

	private Dao dao;
	
	public CampusBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Campus procura(Campus dto) {
		 Campus aux = (Campus) dao.procura(dto.getId(), Campus.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Campus armazena (Campus dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Descricao deve ser informado!");
		/* if (dto.getDatanasc() == null) 
				throw new Exception("O campo Data de Nascimento deve ser informado!"); */		 
		 // colocar aqui as outras validaçoes
		 return (Campus)dao.salva(dto);
	}


	 public List<Object> lista(Campus dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Campus.class);
		
		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("descricao",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
		
		return ct.list();
	 }
	 
	 public void remove(Campus dto) throws Exception {
			dao.remove(procura(dto));
		}
}
