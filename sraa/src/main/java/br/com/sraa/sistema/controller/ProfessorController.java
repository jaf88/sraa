package br.com.sraa.sistema.controller;

import javax.inject.Inject;


import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.AdminController;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.inteceptor.Public;
import br.com.sraa.sistema.bo.CursoBO;
import br.com.sraa.sistema.bo.ProfessorBO;
import br.com.sraa.sistema.model.Curso;
import br.com.sraa.sistema.model.Professor;


@Controller
public class ProfessorController extends DefaultController  {
	
	private ProfessorBO bo;
	
	public ProfessorController(){}
	
	@Inject
	public ProfessorController(Result result,  Dao dao) {
		super(result,dao);
		bo = new ProfessorBO(dao);
	}
	 
	@Public
	@Override
	public void formulario(){
		try {
			result.include("listaCurso",new CursoBO(dao).lista(new Curso()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	public void listar(Professor professor) { 
		 try {
			this.result.include("professor",professor);
			lista =  bo.lista(professor);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	@Public
	public void salvar(Professor professor) {
		try {
			professor = bo.armazena(professor);
			this.result.include("professor",professor);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			this.result.redirectTo(AdminController.class).index();			
		} catch (Exception e) {
			this.result.include("professor",professor);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(ProfessorController.class).formulario();
		}
	}
	
	public void editar(Professor professor) {
		try {
			professor = bo.procura(professor);
			result.include("professor",professor);
			result.include("listaCurso",new CursoBO(dao).lista(new Curso()));
		} catch (Exception e) {
			result.include("professor",professor);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Professor());
		}
	}
	
	public void detalhes(Professor professor) {
		try {
			professor = bo.procura(professor);
			result.include("professor",professor);
		} catch (Exception e) {
			result.include("professor",professor);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Professor());
		}
	}
	
	public void remover(Professor professor) {
		try {
			bo.remove(professor);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(ProfessorController.class).listar(new Professor());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	//Novas fun��es
	public void professordashboard() {}
	public void perfil() {}
	
}