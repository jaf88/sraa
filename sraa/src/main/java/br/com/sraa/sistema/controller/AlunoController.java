package br.com.sraa.sistema.controller;


import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.AdminController;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.inteceptor.Public;
import br.com.sraa.sistema.bo.AlunoAtividadeBO;
import br.com.sraa.sistema.bo.AlunoBO;
import br.com.sraa.sistema.bo.AtividadeBO;
import br.com.sraa.sistema.bo.CursoBO;
import br.com.sraa.sistema.model.Aluno;
import br.com.sraa.sistema.model.AlunoAtividade;
import br.com.sraa.sistema.model.Atividade;
import br.com.sraa.sistema.model.Curso;


@Controller
public class AlunoController extends DefaultController  {
	
	private AlunoBO bo;
	
	public AlunoController(){}
	
	@Inject
	public AlunoController(Result result,  Dao dao) {
		super(result,dao);
		bo = new AlunoBO(dao);
	}
	
	@Public
	@Override
	public void formulario(){
		try {
			result.include("listaCurso",new CursoBO(dao).lista(new Curso()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void listar(Aluno aluno) { 
		 try {
			this.result.include("aluno",aluno);
			lista =  bo.lista(aluno);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	//Novas fun��es
	public void alunodashboard() {}
	
	public void atpadashboard() {
		try {
			result.include("listaAtividade",new AlunoAtividadeBO(dao).listaATPA(new AlunoAtividade()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			this.result.include("msgErro",e.getMessage());
		}
	}
	public void pccsdashboard() {
		try {
			result.include("listaAtividade",new AlunoAtividadeBO(dao).listaPCCS(new AlunoAtividade()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
	public void pccs() {}
	
	public void perfil() {}

	@Public
	public void salvar(Aluno aluno) {
		try {
			aluno = bo.armazena(aluno);
			this.result.include("aluno",aluno);
			this.result.include("msgSucesso","Cadastro Feito com Sucesso\n"
					+ "Confirme o mesmo no seu email!!");
			this.result.redirectTo(AdminController.class).index();
//			this.result.redirectTo(AlunoController.class).detalhes(aluno);
//			result.redirectTo(this).listar(new Aluno());
		} catch (Exception e) {
			this.result.include("aluno",aluno);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(AlunoController.class).formulario();
		}
	}
	
	public void editar(Aluno aluno) {
		try {
			aluno = bo.procura(aluno);
			result.include("aluno",aluno);
			result.include("listaCurso",new CursoBO(dao).lista(new Curso()));
		} catch (Exception e) {
			result.include("aluno",aluno);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Aluno());
		}
	}
	
	public void detalhes(Aluno aluno) {
		try {
			aluno = bo.procura(aluno);
			result.include("aluno",aluno);
		} catch (Exception e) {
			result.include("aluno",aluno);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Aluno());
		}
	}
	
	
	public void remover(Aluno aluno) {
		try {
			bo.remove(aluno);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(AlunoController.class).listar(new Aluno());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
	public void selecionaAtividade(Aluno aluno) {
		
		try {
			aluno = bo.procura(aluno);
			result.include("aluno",aluno);
			result.include("listaatividade",dao.listaTudo(Atividade.class));
		} catch (Exception e) {
			result.include("aluno",aluno);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).detalhes(new Aluno());
		}
	}

	public void salvarAlunoAtividade(AlunoAtividade aluno) {
		try {
//			aluno = bo.armazena(aluno);
			this.result.include("aluno",aluno);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
//			this.result.redirectTo(AlunoController.class).detalhes(aluno);
		} catch (Exception e) {
			this.result.include("aluno",aluno);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(AlunoController.class).formulario();
		}
	}
	
}