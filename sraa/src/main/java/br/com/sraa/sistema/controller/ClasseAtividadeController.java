package br.com.sraa.sistema.controller;



import javax.inject.Inject;

import sun.util.cldr.CLDRLocaleDataMetaInfo;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.bo.ClasseAtividadeBO;
import br.com.sraa.sistema.model.ClasseAtividade;


@Controller
public class ClasseAtividadeController extends DefaultController  {
	
	private ClasseAtividadeBO bo;
	
	public ClasseAtividadeController(){}
	
	@Inject
	public ClasseAtividadeController(Result result,  Dao dao) {
		super(result,dao);
		bo = new ClasseAtividadeBO(dao);
	}
	 
	@Override
	public void formulario(){
		try {
			result.include("listaClasseAtividade",new ClasseAtividadeBO(dao).lista(new ClasseAtividade()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void listar(ClasseAtividade classeAtividade) { 
		 try {
			this.result.include("classeAtividade",classeAtividade);
			lista =  bo.lista(classeAtividade);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	
	public void salvar(ClasseAtividade classeAtividade) {
		try {
			classeAtividade = bo.armazena(classeAtividade);
			this.result.include("classeAtividade",classeAtividade);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(this).listar(new ClasseAtividade());
//			this.result.redirectTo(ClasseAtividadeController.class).detalhes(classeAtividade);
		} catch (Exception e) {
			this.result.include("classeAtividade",classeAtividade);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(ClasseAtividadeController.class).formulario();
		}
	}
	
	public void editar(ClasseAtividade classeAtividade) {
		try {
			classeAtividade = bo.procura(classeAtividade);
			result.include("classeAtividade",classeAtividade);
		} catch (Exception e) {
			result.include("classeAtividade",classeAtividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new ClasseAtividade());
		}
	}
	
	public void detalhes(ClasseAtividade classeAtividade) {
		try {
			classeAtividade = bo.procura(classeAtividade);
			result.include("classeAtividade",classeAtividade);
		} catch (Exception e) {
			result.include("classeAtividade",classeAtividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new ClasseAtividade());
		}

	}
	
	public void remover(ClasseAtividade classeAtividade) {
		try {
			bo.remove(classeAtividade);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(ClasseAtividadeController.class).listar(new ClasseAtividade());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
}