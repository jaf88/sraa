package br.com.sraa.sistema.bo;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.model.Categoria;



public class CategoriaBO {

	private Dao dao;
	
	public CategoriaBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Categoria procura(Categoria dto) {
		 Categoria aux = (Categoria) dao.procura(dto.getId(), Categoria.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Categoria armazena (Categoria dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Descri��o deve ser informado!");
		/* if (dto.getDatanasc() == null) 
				throw new Exception("O campo Data de Nascimento deve ser informado!"); */		 
		 // colocar aqui as outras valida�oes
		 		 
		 return (Categoria)dao.salva(dto);
	}


	 public List<Object> lista(Categoria dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Categoria.class);

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
		
		return ct.list();
	 }
	 
	 public void remove(Categoria dto) throws Exception {
			dao.remove(procura(dto));
		}
	 
}
