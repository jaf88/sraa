package br.com.sraa.sistema.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


@Entity
//@Table (name="categoria")
public class Categoria {

	private Long id;
	private ClasseAtividade classeAtividade;
	private String descricao;
	private String sigla;
	private Integer status;
	
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

//	@OneToMany(cascade = {}, fetch = FetchType.LAZY)
	@OneToOne(cascade = {}, fetch = FetchType.LAZY) //Funcionava com esse mapeamento!
	@JoinColumn(name = "idclasseatividade", nullable = false)
	public ClasseAtividade getClasseAtividade() {
		return classeAtividade;
	}
	public void setClasseAtividade(ClasseAtividade classeAtividade) {
		this.classeAtividade = classeAtividade;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

}
