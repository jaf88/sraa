package br.com.sraa.sistema.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.bo.CampusBO;
import br.com.sraa.sistema.model.Campus;

@Controller
public class CampusController extends DefaultController  {
	
	private CampusBO bo;
	
	public CampusController(){}
	
	@Inject
	public CampusController(Result result,  Dao dao) {
		super(result,dao);
		bo = new CampusBO(dao);
	}
				
	
	public void listar(Campus campus) { 
		 try {
			this.result.include("campus",campus);
			lista =  bo.lista(campus);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	
	public void salvar(Campus campus) {
		try {
			campus = bo.armazena(campus);
			this.result.include("campus",campus);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
//			this.result.redirectTo(MainController.class).dashboard();
			//			result.redirectTo(this).listar(new Campus());
//			this.result.redirectTo(CampusController.class).listar(campus);
			result.redirectTo(this).listar(new Campus());
		} catch (Exception e) {
			this.result.include("campus",campus);
			this.result.include("msgErro",e.getMessage());
//			this.result.redirectTo(CampusController.class).formulario();
		}
	}
	
	public void editar(Campus campus) {
		try {
			campus = bo.procura(campus);
			result.include("campus",campus);
		} catch (Exception e) {
			result.include("campus",campus);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Campus());
		}
	}
	
	public void detalhes(Campus campus) {
		try {
			campus = bo.procura(campus);
			result.include("campus",campus);
		} catch (Exception e) {
			result.include("campus",campus);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Campus());
		}

	}
	
	public void remover(Campus campus) {
		try {
			bo.remove(campus);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(CampusController.class).listar(new Campus());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
}