package br.com.sraa.sistema.bo;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.model.ClasseAtividade;


public class ClasseAtividadeBO {

	private Dao dao;
	
	public ClasseAtividadeBO(Dao dao) {
		this.dao=dao;
	}
	
	 public ClasseAtividade procura(ClasseAtividade dto) {
		 ClasseAtividade aux = (ClasseAtividade) dao.procura(dto.getId(), ClasseAtividade.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public ClasseAtividade armazena (ClasseAtividade dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Descricao deve ser informado!");
		/* if (dto.getDatanasc() == null) 
				throw new Exception("O campo Data de Nascimento deve ser informado!"); */		 
		 // colocar aqui as outras validaçoes
		 return (ClasseAtividade)dao.salva(dto);
	}


	 public List<Object> lista(ClasseAtividade dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(ClasseAtividade.class);

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
		

		return ct.list();
	 }
	 
	 public void remove(ClasseAtividade dto) throws Exception {
			dao.remove(procura(dto));
		}
}
