package br.com.sraa.sistema.model;


import javax.persistence.*;

import br.com.sraa.geral.model.Grupousuario;
import br.com.sraa.geral.model.Permissao;

@Entity
@Table (name="professor")
public class Professor {

	private Long id;
	private String nome;
	private Curso curso;
	private String email;
	private String cpf;
	private String senha;
	private String sexo;
	private String telefone;
	private Integer status;
	private Grupousuario grupousuario;
	
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@OneToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "idgrupousuario", nullable = false)
	public Grupousuario getGrupousuario() {
		return grupousuario;
	}
	public void setGrupousuario(Grupousuario grupousuario) {
		this.grupousuario = grupousuario;
	}
	
	@OneToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "idcurso", nullable = false)
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	@Transient
	public boolean isAutorizado(String url){
		
		if(this.grupousuario.getId().equals(1l)) // se for do grupo Administrador
			return true;
		
		for (Permissao permissao : this.grupousuario.getPermissoes()) {
			if(permissao.getDescricao().equals(url))
				return true;
		}
		return false;
	}
}
