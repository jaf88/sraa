package br.com.sraa.sistema.controller;


import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.controller.MainController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.bo.CampusBO;
import br.com.sraa.sistema.bo.CursoBO;
import br.com.sraa.sistema.model.Campus;
import br.com.sraa.sistema.model.Curso;


@Controller
public class CursoController extends DefaultController  {
	
	private CursoBO bo;
	
	public CursoController(){}
	
	@Inject
	public CursoController(Result result,  Dao dao) {
		super(result,dao);
		bo = new CursoBO(dao);
	}
				
	@Override
	public void formulario() {
		try {
			result.include("listaCampus",new CampusBO(dao).lista(new Campus()));	
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void listar(Curso curso) { 
		 try {
			this.result.include("curso",curso);
			lista =  bo.lista(curso);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	public void salvar(Curso curso) {
		try {
			curso = bo.armazena(curso);
			this.result.include("curso",curso);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
//			this.result.redirectTo(MainController.class).dashboard();
			result.redirectTo(this).listar(new Curso());
//			this.result.redirectTo(CursoController.class).detalhes(curso);
		} catch (Exception e) {
			this.result.include("curso",curso);
			this.result.include("msgErro",e.getMessage());
//			this.result.redirectTo(CursoController.class).formulario();
		}
	}
	
	public void editar(Curso curso) {
		try {
			curso = bo.procura(curso);
			result.include("curso",curso);
		} catch (Exception e) {
			result.include("curso",curso);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Curso());
		}
	}
	
	public void detalhes(Curso curso) {
		try {
			curso = bo.procura(curso);
			result.include("curso",curso);
		} catch (Exception e) {
			result.include("curso",curso);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Curso());
		}

	}
	
	public void remover(Curso curso) {
		try {
			bo.remove(curso);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(CursoController.class).listar(new Curso());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
}