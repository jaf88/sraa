package br.com.sraa.sistema.bo;



import java.util.List;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.util.JavaMailApp;
import br.com.sraa.sistema.model.Professor;


public class ProfessorBO {

	private Dao dao;
	JavaMailApp javaMailApp = new JavaMailApp();
	private Random  rangeProfessor = new Random();
	
	public ProfessorBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Professor procura(Professor dto) {
		 Professor aux = (Professor) dao.procura(dto.getId(), Professor.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Professor armazena (Professor dto)  throws Exception {
		 if (dto.getNome() == null || dto.getNome().equals("")) 
				throw new Exception("O campo Nome deve ser informado!");
		 dto.setSenha(dto.getSenha().hashCode()+"");           
		 javaMailApp.enviaEmail(dto.getEmail(), dto.getNome(), dto.getGrupousuario().getId());
		 
		 return (Professor)dao.salva(dto);
	}


	 public List<Object> lista(Professor dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Professor.class);

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getNome() != null && !dto.getNome().equals("")) 
			ct.add(Restrictions.like("nome",dto.getNome(),MatchMode.ANYWHERE).ignoreCase());
		
		return ct.list();
	 }

	
//		public List<Object> professorRange() throws Exception {
//			Criteria ct = this.dao.getSession().createCriteria(Professor.class);
//			List listaSort = ct.list();
//			int sorteado = rangeProfessor.nextInt(listaSort.size());
//			ct.add(Restrictions.eq("id", sorteado));
//		
//			return ct.list();
//		}
		
//		public Professor rangeProfessor(Aluno dto) throws Exception {
//			Aluno auxdto = null;
//			try {
//				Criteria sa = this.dao.getSession().createCriteria(Aluno.class);
//				if (dto.getMatricula() != null && dto.getSenha() != null) {
//					sa.add(Restrictions.eq("matricula", dto.getMatricula()));
////					sa.add(Restrictions.eq("status", null));
//					List<Aluno> list = (List<Aluno>) sa.list();
//					if (list.size() > 0) {
//						auxdto = (Aluno) list.get(0);
//						// cria o codigo hash da senha digitada digitada no formul�rio
//						String senhaDigitada = dto.getSenha().hashCode() + "";
//						auxdto.setStatus(1);
//						
//						// compara com a senha do banco, retorna o usuario n�o forem iguais e null caso
//						// contr�rio
//						return (senhaDigitada.equals(auxdto.getSenha()) ? ((Aluno)dao.salva(auxdto)) : null);
//					} 
//					
//				}
//			} catch (Exception e) {
//				throw new Exception("Erro ao efetuar login: " + e.getMessage());
//			}
//			return auxdto;
//		}
	 
	public void remove(Professor dto) throws Exception {
			dao.remove(procura(dto));
	}
	
	public Professor autenticaPrimeiroLogin(Professor dto) throws Exception {
		Professor auxdto = null;
		try {
			Criteria sa = this.dao.getSession().createCriteria(Professor.class);
			if (dto.getCpf() != null && dto.getSenha() != null) {
				sa.add(Restrictions.eq("cpf", dto.getCpf()));
//				sa.add(Restrictions.eq("status", null));
				List<Professor> list = (List<Professor>) sa.list();
				if (list.size() > 0) {
					auxdto = (Professor) list.get(0);
					// cria o codigo hash da senha digitada digitada no formul�rio
					String senhaDigitada = dto.getSenha().hashCode() + "";
					auxdto.setStatus(1);
					
					// compara com a senha do banco, retorna o usuario n�o forem iguais e null caso
					// contr�rio
					return (senhaDigitada.equals(auxdto.getSenha()) ? ((Professor)dao.salva(auxdto)) : null);
				} 
				
			}
		} catch (Exception e) {
			throw new Exception("Erro ao efetuar login: " + e.getMessage());
		}
		return auxdto;
	}
	public Professor autenticaLogin(Professor dto) throws Exception {
		Professor auxdto = null;
		try {
			Criteria sa = this.dao.getSession().createCriteria(Professor.class);
			if (dto.getCpf() != null && dto.getSenha() != null) {
				sa.add(Restrictions.eq("cpf", dto.getCpf()));
				sa.add(Restrictions.eq("status", 1));
				List<Professor> list = (List<Professor>) sa.list();
				if (list.size() > 0) {
					auxdto = (Professor) list.get(0);
					// cria o codigo hash da senha digitada digitada no formul�rio
					String senhaDigitada = dto.getSenha().hashCode() + "";
					// compara com a senha do banco, retorna o usuario n�o forem iguais e null caso
					// contr�rio
					return (senhaDigitada.equals(auxdto.getSenha()) ? auxdto : null);
				} 
				
			}
		} catch (Exception e) {
			throw new Exception("Erro ao efetuar login: " + e.getMessage());
		}
		return auxdto;
	} 
}
