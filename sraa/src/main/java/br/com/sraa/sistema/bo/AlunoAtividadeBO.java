package br.com.sraa.sistema.bo;

import java.util.List;
import java.util.Random;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.model.AlunoAtividade;


public class AlunoAtividadeBO {
	private Dao dao;
	Random  rangeProfessor = new Random();
	
	public AlunoAtividadeBO(Dao dao) {
		this.dao = dao;
	}

	public AlunoAtividade procura(AlunoAtividade dto) {
		AlunoAtividade aux = (AlunoAtividade) dao.procura(dto.getId(), AlunoAtividade.class);
		return (aux.getId() != null ? aux : null);
	}

	public AlunoAtividade armazena(AlunoAtividade dto) throws Exception {
		
		if (dto.getDescricao() == null || dto.getDescricao().equals(""))
			throw new Exception("O campo Nome deve ser informado!");
//		dto.setProfessor(professorRando());
		return (AlunoAtividade) dao.salva(dto);
	}
	
	//Fun��o feita com o intuito de sortear um professor para uma atividade, sem a nescessidade
	// do administrador setar manualmente um professor para avaliar a atividade - a fun��o funciona,
	// mais foi recomendado que um professor seja responsavel por um aluno, ao inv�s de v�rias
	//atividades de diferentes alunos.
	
	/* public Professor professorRando() {
		Professor p = null;
		Criteria ct = dao.getSession().createCriteria(Professor.class);
		Long sorteado =  (long) rangeProfessor.nextInt(ct.list().size());
		if(sorteado==0)
			sorteado+=1;
		ct.add(Restrictions.eq("id", sorteado));
		List<Professor> list = (List<Professor>) ct.list();
		p = (Professor) list.get(0);
		return p;
	 }*/
	 
	

	public List<Object> lista(AlunoAtividade dto) throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(AlunoAtividade.class);
		
		if (dto.getId() != null)
			ct.add(Restrictions.eq("id", dto.getId()));

		if (dto.getDescricao() != null && !dto.getDescricao().equals(""))
			ct.add(Restrictions.like("nome", dto.getDescricao(), MatchMode.ANYWHERE).ignoreCase());

		return ct.list();
	}
	
	public List<Object> listaATPA(AlunoAtividade dto) throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(AlunoAtividade.class);
		ct.add(Restrictions.lt("atividade.id", new Long(17)));
		if (dto.getId() != null)
			ct.add(Restrictions.eq("id", dto.getId()));

		if (dto.getDescricao() != null && !dto.getDescricao().equals(""))
			ct.add(Restrictions.like("nome", dto.getDescricao(), MatchMode.ANYWHERE).ignoreCase());

		return ct.list();
	}
	
	public List<Object> listaPCCS(AlunoAtividade dto) throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(AlunoAtividade.class);
		ct.add(Restrictions.gt("atividade.id", new Long(16)));
		if (dto.getId() != null)
			ct.add(Restrictions.eq("id", dto.getId()));

		if (dto.getDescricao() != null && !dto.getDescricao().equals(""))
			ct.add(Restrictions.like("nome", dto.getDescricao(), MatchMode.ANYWHERE).ignoreCase());

		return ct.list();
	}

	public void remove(AlunoAtividade dto) throws Exception {
		dao.remove(procura(dto));
	}

//	 public AlunoAtividade armazenaProfessorAvaliador (Professor professor, AlunoAtividade alunoAtividade)  throws Exception {
//		 if (professor.getId() == null || professor.getId()==0)  
//				throw new Exception("O campo Permiss�o deve ser informado!");
//		 alunoAtividade = this.procura(alunoAtividade);
//		 alunoAtividade.getAvaliarprofessor().add(professor);
//		 return (AlunoAtividade)dao.salva(alunoAtividade);
//	}	 
//	 
	 
//	 public Grupousuario removePermissao (Permissao permissao, Grupousuario grupousuario)  throws Exception {
//		 grupousuario = this.procura(grupousuario);
//		 permissao = (Permissao)dao.procura(permissao.getId(), Permissao.class);
//		 grupousuario.getPermissoes().remove(permissao);
//		 return (Grupousuario)dao.salva(grupousuario);
//	}	
}
