package br.com.sraa.sistema.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.AdminController;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.controller.GrupousuarioController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.inteceptor.Public;
import br.com.sraa.geral.model.Grupousuario;
import br.com.sraa.geral.model.Permissao;
import br.com.sraa.sistema.bo.AlunoAtividadeBO;
import br.com.sraa.sistema.bo.AtividadeBO;
import br.com.sraa.sistema.bo.CursoBO;
import br.com.sraa.sistema.model.AlunoAtividade;
import br.com.sraa.sistema.model.Atividade;
import br.com.sraa.sistema.model.Curso;
import br.com.sraa.sistema.model.Professor;

@Controller
public class AlunoAtividadeController extends DefaultController{
	
private AlunoAtividadeBO bo;
	
	public AlunoAtividadeController(){}
	
	@Inject
	public AlunoAtividadeController(Result result,  Dao dao) {
		super(result,dao);
		bo = new AlunoAtividadeBO(dao);
	}
	
//	@Public
	public void formularioATPA(){
		try {
			result.include("listaAtividade",new AtividadeBO(dao).listaATPA(new Atividade()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void formularioPCCS(){
		try {
			result.include("listaAtividade",new AtividadeBO(dao).listaPCCS(new Atividade()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void listar(AlunoAtividade alunoAtividade) { 
		 try {
			this.result.include("aluno",alunoAtividade);
			lista =  bo.lista(alunoAtividade);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	public void listarATPA(AlunoAtividade alunoAtividade) { 
		 try {
			this.result.include("aluno",alunoAtividade);
			lista =  bo.listaATPA(alunoAtividade);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	public void listarPCCS(AlunoAtividade alunoAtividade) { 
		 try {
			this.result.include("aluno",alunoAtividade);
			lista =  bo.listaPCCS(alunoAtividade);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	@Public
	public void salvar(AlunoAtividade alunoAtividade) {
		try {
			alunoAtividade = bo.armazena(alunoAtividade);
			this.result.include("alunoAtividade",alunoAtividade);
			if(alunoAtividade.getAtividade().getId() < new Long(17))
				this.result.redirectTo(AlunoController.class).atpadashboard();
			else
				this.result.redirectTo(AlunoController.class).pccsdashboard();
//			this.result.redirectTo(AlunoController.class).alunodashboard();

		} catch (Exception e) {
			this.result.include("alunoAtividade",alunoAtividade);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(AlunoAtividadeController.class).formulario();
		}
	}
	
	public void editar(AlunoAtividade alunoAtividade) {
		try {
			alunoAtividade = bo.procura(alunoAtividade);
			result.include("alunoAtividade",alunoAtividade);
			result.include("listaCurso",new CursoBO(dao).lista(new Curso()));
		} catch (Exception e) {
			result.include("alunoAtividade",alunoAtividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new AlunoAtividade());
		}
	}
	
	public void detalhes(AlunoAtividade alunoAtividade) {
		try {
			alunoAtividade = bo.procura(alunoAtividade);
			result.include("alunoAtividade",alunoAtividade);
		} catch (Exception e) {
			result.include("alunoAtividade",alunoAtividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new AlunoAtividade());
		}
	}
	
	public void detalheATPA(AlunoAtividade alunoAtividade) {
		try {
			alunoAtividade = bo.procura(alunoAtividade);
			result.include("alunoAtividade",alunoAtividade);
		} catch (Exception e) {
			result.include("alunoAtividade",alunoAtividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new AlunoAtividade());
		}
	}
	
	public void detalhePCCS(AlunoAtividade alunoAtividade) {
		try {
			alunoAtividade = bo.procura(alunoAtividade);
			result.include("alunoAtividade",alunoAtividade);
		} catch (Exception e) {
			result.include("alunoAtividade",alunoAtividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new AlunoAtividade());
		}
	}
	
	public void remover(AlunoAtividade alunoAtividade) {
		try {
			bo.remove(alunoAtividade);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(AlunoAtividadeController.class).listar(new AlunoAtividade());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
//	public void salvarProfessorAvaliador(Professor professor, AlunoAtividade alunoAtividade) {
//		try {
//			alunoAtividade = this.bo.armazenaProfessorAvaliador(professor, alunoAtividade);
//			this.result.include("msgSucesso","Registro Salvo com sucesso!");
////			result.redirectTo(GrupousuarioController.class).detalhes(grupousuario);
//		} catch (Exception e) {
//			this.result.include("msgErro",e.getMessage());
////			result.redirectTo(GrupousuarioController.class).detalhes(grupousuario);
//		}
//	}
	
//	public void selecionaAtividade(Aluno aluno) {
//		
//		try {
//			aluno = bo.procura(aluno);
//			result.include("aluno",aluno);
//			result.include("listaatividade",dao.listaTudo(Atividade.class));
//		} catch (Exception e) {
//			result.include("aluno",aluno);
//			this.result.include("msgErro",e.getMessage());
//			result.redirectTo(this).detalhes(new Aluno());
//		}
//	}
//
//	public void salvarAlunoAtividade(AlunoAtividade aluno) {
//		try {
////			aluno = bo.armazena(aluno);
//			this.result.include("aluno",aluno);
//			this.result.include("msgSucesso","Registro Salvo com sucesso!");
////			this.result.redirectTo(AlunoController.class).detalhes(aluno);
//		} catch (Exception e) {
//			this.result.include("aluno",aluno);
//			this.result.include("msgErro",e.getMessage());
//			this.result.redirectTo(AlunoController.class).formulario();
//		}
//	}

}
