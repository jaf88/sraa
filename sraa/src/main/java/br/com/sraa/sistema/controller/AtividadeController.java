package br.com.sraa.sistema.controller;


import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.controller.DefaultController;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.bo.AtividadeBO;
import br.com.sraa.sistema.bo.CategoriaBO;
import br.com.sraa.sistema.model.Atividade;
import br.com.sraa.sistema.model.Categoria;




@Controller
public class AtividadeController extends DefaultController  {
	
	private AtividadeBO bo;
	
	public AtividadeController(){}
	
	@Inject
	public AtividadeController(Result result,  Dao dao) {
		super(result,dao);
		bo = new AtividadeBO(dao);
	}
	 
	@Override
	public void formulario(){
		try {
			result.include("listaCategoria",new CategoriaBO(dao).lista(new Categoria()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void listar(Atividade atividade) { 
		 try {
			this.result.include("atividade",atividade);
			lista =  bo.lista(atividade);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	
	public void salvar(Atividade atividade) {
		try {
			atividade = bo.armazena(atividade);
			this.result.include("atividade",atividade);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(this).listar(new Atividade());
//			this.result.redirectTo(AtividadeController.class).detalhes(atividade);
		} catch (Exception e) {
			this.result.include("atividade",atividade);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(AtividadeController.class).formulario();
		}
	}
	
	public void editar(Atividade atividade) {
		try {
			atividade = bo.procura(atividade);
			result.include("atividade",atividade);
			result.include("listaCategoria",new CategoriaBO(dao).lista(new Categoria()));
		} catch (Exception e) {
			result.include("atividade",atividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Atividade());
		}
	}
	
	public void detalhes(Atividade atividade) {
		try {
			atividade = bo.procura(atividade);
			result.include("atividade",atividade);
		} catch (Exception e) {
			result.include("atividade",atividade);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Atividade());
		}

	}
	
	public void remover(Atividade atividade) {
		try {
			bo.remove(atividade);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(AtividadeController.class).listar(new Atividade());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
}