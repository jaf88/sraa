package br.com.sraa.sistema.bo;




import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.sistema.model.Atividade;



public class AtividadeBO {

	private Dao dao;
	
	public AtividadeBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Atividade procura(Atividade dto) {
		 Atividade aux = (Atividade) dao.procura(dto.getId(), Atividade.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Atividade armazena (Atividade dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Nome da Atividade deve ser informado!");
		/* if (dto.getDatanasc() == null) 
				throw new Exception("O campo Data de Nascimento deve ser informado!"); */		 
		 // colocar aqui as outras valida�oes
		 return (Atividade)dao.salva(dto);
	}


	 public List<Object> lista(Atividade dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Atividade.class);
		
		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());

		return ct.list();
	 }
	 
	 //Observa��es - Foi usado criteria para retornar as ATPA e PCCS em selects separados,
	 //Como no banco dados o casdastro da atpa vai at� a o id= 16 e as pccs come�am a partir do do id=17,
	 //caso seja acrescentado algum tipo de atividade a mais, o codigo deve ser refatorado nas fun��es
	 //de listaATPA e listaPCCS
	 
	 public List<Object> listaATPA(Atividade dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Atividade.class);
		ct.add(Restrictions.lt("id", new Long(17)));

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());

		return ct.list();
	 }
	 
	 public List<Object> listaPCCS(Atividade dto)throws Exception {
			Criteria ct = this.dao.getSession().createCriteria(Atividade.class);
			ct.add(Restrictions.gt("id", new Long(16)));

			if (dto.getId()!= null) 
				ct.add(Restrictions.eq("id",dto.getId()));

			
			if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
				ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
			

			return ct.list();
		 }
	 
	 public void remove(Atividade dto) throws Exception {
			dao.remove(procura(dto));
		}
}
