package br.com.sraa.geral.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import br.com.sraa.geral.dao.Dao;


public class Reflex {
	
	public static final Object atualiza(Object dto,Dao dao) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	    Class classeBase = dto.getClass();
	    Object dtoBanco = dto;
	    if(!isNovo(dto)){
	    	// Busca o objeto do banco de forma generica
	    	dtoBanco = dao.procura((Long)pegaValor(dto,"id"), classeBase);
	    	Method metodoGet =null;
	    	Object valorRaptor=null;
	    	Object valorBanco=null;
	    	boolean iguais = true;
	    	Object[] parametroGet  = {};
	    	Object[] parametroSet = {null};
	    	for(Class classe = classeBase; classe != null; classe = classe.getSuperclass()) // varre todos os atributos da classe
	    		for(Field atributo : classe.getDeclaredFields()){
	    			iguais = false;
	    			metodoGet = getMetodo(classeBase, atributo.getName(), "get"); //pega o metodo get do atributo
	    			// 	valorRaptor � o valor que veio da view
	    			valorRaptor = metodoGet.invoke(dto, parametroGet); //invoca o metodo get do atributo e pega seu valor
	    			// valorBanco � o valor que est� no Banco de Dados
	    			valorBanco = metodoGet.invoke(dtoBanco, parametroGet); //invoca o metodo get do atributo e pega seu valor
	    			if(valorBanco!=null)// valorBanco pode ser nulo no Banco de Dados
	    				iguais = valorBanco.equals(valorRaptor);
	    			// Se o valorRaptor for nulo significa que o campo desse atributo n�o estava no formul�rio ent�o ele
	    			// 	� um campo de controle ou seja � atualizado internamente pelo proprio sistema
	    			// Atributos do tipo Set n�o n�o podem ser atualizados pois esses n�o v�o para a vis�o
	    			// E s� atualizaremos os atributos com valores diferentes diminuindo o overhead
	    			if(valorRaptor!=null && atributo.getType()!= Set.class && !iguais ){
	    				parametroSet[0] = valorRaptor;
	    				// 	invoca o metodo set do atributo persistente(banco) passando o valor do atributo transiente(view) 
	    				Method metodo = getMetodo(classeBase, atributo.getName(), "set");
	    				if(metodo!=null)
	    					metodo.invoke(dtoBanco, parametroSet);
	    			}	
	    		}
	     }
	    return dtoBanco;
	  }

	/*
	 *Retorna todos os metodos de uma classe de um determinado tipo(get ou set)
	 */
	public Method[] getMetodos(Class classe,String tipo){
		Method[] metodos;
		ArrayList<Method> arraymetodo = new ArrayList<Method>();
		int i=0;
		for(Method metodo : classe.getMethods()){
			if(metodo.getName().substring(0,3).equals(tipo)&& !metodo.getName().equals("getClass")) // menos o m�todo getClass
				arraymetodo.add(metodo);
		}
		metodos = new Method[arraymetodo.size()];
		for(Method metodo : arraymetodo)
			metodos[i++]=metodo;
		return metodos;
	}
	
	/*
	 * Retorna os metodos Get e Set - M�todos Acessores  do atributo de uma classe
	 */

	public Method[] getAcessores(Class classe,String atributo){
		Method[] metodos;
		ArrayList<Method> arraymetodo = new ArrayList<Method>();
		int i=0;
		for(Method metodo : classe.getMethods()){
			// verifica se o metodo contem o nome do atributo
			if(metodo.getName().substring(0).toLowerCase().contains(atributo.toLowerCase()))
				arraymetodo.add(metodo);
		}
		metodos = new Method[arraymetodo.size()];
		for(Method metodo : arraymetodo)
			metodos[i++]=metodo;
		return metodos;
	}

	/*
	 * Retorna um m�todo especifico get/set do atributo de uma classe
	 */
	public static Method getMetodo(Class classe,String atributo,String tipo){ 
		Method m = null;
		for(Method metodo : classe.getMethods()){
			if(metodo.getName().substring(3).toLowerCase().equals(atributo.toLowerCase())
			 && ( (metodo.getName().substring(0,3).equals(tipo) || (metodo.getName().substring(0,2).equals("is")) )  
			 && !metodo.getName().equals("getClass"))){
				m = metodo;
				break;
			 }	
		}
		return m;
	}

		
	/*
	 * Verifica se um objeto est� transiente ou persistente 
	 */
	public static boolean isNovo(Object dto)throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	    Object valor = pegaValor(dto,"id"); // se existir uma valor para o ID � persistente
	    return valor==null || ((Long)valor).intValue()==0; 
	}
	
	/*
	 * Verifica se um objeto � nulo, nesse caso, se n�o existe ID
	 * � utilizado em algumas Criterias
	 */
	public static boolean isNull(Object dto)throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	    if(dto==null)
	    	return true;
	    Object valor = pegaValor(dto,"id");
	    return valor == null || ((Long)valor).intValue()==0; 
	}

	/*
	 * Retorna o valor da chave ID de uma determinado objeto
	 */

	public static Object pegaValor(Object dto,String campo) throws IllegalAccessException, InvocationTargetException {
		Class classeBase = dto.getClass();
	    Object[] parametroGet  = {};
	    Method metodoGet = getMetodo(classeBase, campo, "get");
	    Object valor = null;
	    if (metodoGet!=null)
	    	valor = metodoGet.invoke(dto, parametroGet);
		return valor;
	}
	

}
 

	