package br.com.sraa.geral.controller;

import java.util.List;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.inteceptor.SessaoInfo;

@Controller
public class DefaultController {

	protected final Result result;
	
	protected final Dao dao;
	
	protected final SessaoInfo sessaoInfo;
	
	protected List<Object> lista;
	public DefaultController() {
		this.result = null;
		this.dao=null;
		this.sessaoInfo=null;
	}
	
	
	public DefaultController(Result result,Dao dao,SessaoInfo sessaoInfo) {
		this.result = result;
		this.dao=dao;
		this.sessaoInfo=sessaoInfo;
	}
	
	
	public DefaultController(Result result,Dao dao) {
		this.result = result;
		this.dao=dao;
		this.sessaoInfo=null;
	}
	
	public void formulario(){}
	
}