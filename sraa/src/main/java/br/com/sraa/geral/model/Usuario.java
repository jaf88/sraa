package br.com.sraa.geral.model;
import java.io.Serializable;

import javax.persistence.*;

import br.com.sraa.sistema.model.Aluno;
import br.com.sraa.sistema.model.Professor;

@Entity
@Table (name="usuario")
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = 773496171272411714L;
	
	private Long id;
	private Grupousuario grupousuario;
	private String descricao;
	private String	login;
	private String senha;
	@Transient 
	private String senhaConf;
	private Integer status;
	
	@Transient
	private Aluno aluno;
	@Transient
	private Professor professor;
	
	@Transient
	public Aluno getAluno() {
		return aluno;
	}
	@Transient
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	
	@Transient
	public Professor getProfessor() {
		return professor;
	}
	@Transient
	public void setProfessor(Professor professoro) {
		this.professor = professoro;
	}
	
	@Id @GeneratedValue (strategy = GenerationType.IDENTITY) 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	@Transient
	public String getSenhaConf() {
		return senhaConf;
	}
	public void setSenhaConf(String senhaConf) {
		this.senhaConf = senhaConf;
	}
	
	@JoinColumn(name = "idgrupoUsuario", referencedColumnName = "id")
    @ManyToOne		
	public Grupousuario getGrupousuario() {
		return this.grupousuario;
	}

	public void setGrupousuario(Grupousuario grupousuario) {
		this.grupousuario = grupousuario;
	}
	
	@Transient
	public boolean isAutorizado(String url){
		
		if(this.grupousuario.getId().equals(1l)) // se for do grupo Administrador
			return true;
		
		for (Permissao permissao : this.grupousuario.getPermissoes()) {
			if(permissao.getDescricao().equals(url))
				return true;
		}
		return false;
	}
	
}