package br.com.sraa.geral.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.bo.UsuarioBO;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.inteceptor.Public;
import br.com.sraa.geral.inteceptor.SessaoInfo;
import br.com.sraa.geral.model.Usuario;
import br.com.sraa.sistema.bo.AlunoBO;
import br.com.sraa.sistema.bo.ProfessorBO;
import br.com.sraa.sistema.controller.AlunoController;
import br.com.sraa.sistema.controller.ProfessorController;
import br.com.sraa.sistema.model.Aluno;
import br.com.sraa.sistema.model.Professor;


@Controller
public class AdminController extends DefaultController  {

	private UsuarioBO bo;
	private AlunoBO boaluno;
	private ProfessorBO boprofessor;

	public AdminController() {}
	
	@Inject
	public AdminController(Result result, Dao dao, SessaoInfo sessaoInfo) {		
		super(result,dao,sessaoInfo);
		bo = new UsuarioBO(dao);
		boaluno = new AlunoBO(dao);
		boprofessor = new ProfessorBO(dao);
	}
		
	@Public
	@Path("/")
	public void index() {}

	@Public
	public void efetuaLogin(Usuario usuario) {
		try {
			Usuario logado = bo.autentica(usuario);
			sessaoInfo.login(logado);
			if (logado == null) {
				 result.include("msgErro","Usuario e/ou senha invalidos!");
				 result.redirectTo(AdminController.class).index();
			} else {
				 result.redirectTo(MainController.class).dashboard();
			}
		} catch (Exception e) {
			result.include("msgErro",e.toString());
			result.redirectTo(AdminController.class).index();
//			result.redirectTo(MainController.class).dashboard();	
		}
	}
	
	@Public
	public void confirmacadastro() {
		result.include("msgSucesso","Quase l�, fa�a o login para confirmar seu cadastro!!");
	}
	

	@Public
	public void confirmacadastroprofessor() {
		result.include("msgSucesso","Quase l�, fa�a o login para confirmar seu cadastro!!");
	}
	
	@Public
	public void efetuaLoginAluno(Aluno aluno) {
		try {
			Aluno logado = boaluno.autenticaPrimeiroLogin(aluno);
			sessaoInfo.loginAluno(logado);
			if (logado == null) {
				 result.include("msgErro","Usuario j� confirmado, fa�a o seu Login Agora!");
				 result.redirectTo(AdminController.class).index();
			} else {
				 result.redirectTo(AlunoController.class).alunodashboard();
			}
		} catch (Exception e) {
			result.include("msgErro",e.toString());
			result.redirectTo(AdminController.class).index();
//			result.redirectTo(MainController.class).dashboard();	
		}
	}
	
	@Public
	public void efetuaLoginProfessor(Professor professor) {
		try {
			Professor logado = boprofessor.autenticaPrimeiroLogin(professor);
			sessaoInfo.loginProfessor(logado);
			if (logado == null) {
				 result.include("msgErro","Usuario j� confirmado, fa�a o seu Login Agora!");
				 result.redirectTo(AdminController.class).index();
			} else {
				 result.redirectTo(ProfessorController.class).professordashboard();
			}
		} catch (Exception e) {
			result.include("msgErro",e.toString());
			result.redirectTo(AdminController.class).index();
		}
	}
	
	@Public
	public void efetuaLoginProfessorGeral(Professor professor) {
		try {
			Professor logado = boprofessor.autenticaLogin(professor);
			sessaoInfo.loginProfessor(logado);
			if (logado == null) {
				 result.include("msgErro","Usuario e/ou senha invalidos!");
				 result.redirectTo(AdminController.class).index();
			} else {
				 result.redirectTo(ProfessorController.class).professordashboard();
			}
		} catch (Exception e) {
			result.include("msgErro",e.toString());
			result.redirectTo(AdminController.class).index();
//			result.redirectTo(MainController.class).dashboard();	
		}
	}
	
	@Public
	public void efetuaLoginAlunoGeral(Aluno aluno) {
		try {
			Aluno logado = boaluno.autenticaLogin(aluno);
			sessaoInfo.loginAluno(logado);
			if (logado == null) {
				 result.include("msgErro","Usuario e/ou senha invalidos!");
				 result.redirectTo(AdminController.class).index();
			} else {
				 result.redirectTo(AlunoController.class).alunodashboard();
			}
		} catch (Exception e) {
			result.include("msgErro",e.toString());
			result.redirectTo(AdminController.class).index();
//			result.redirectTo(MainController.class).dashboard();	
		}
	}
	public void logout() {
		this.index();
	}
	
	
}