package br.com.sraa.geral.inteceptor;


import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.sraa.geral.model.Usuario;
import br.com.sraa.sistema.model.Aluno;
import br.com.sraa.sistema.model.Professor;



@SessionScoped
@Named
public class SessaoInfo implements Serializable {

	private static final long serialVersionUID = 773496171272411714L;

	private Usuario logado;
	private Aluno logaluno;
	private Professor logaprofessor;
	private Date dataAtual;


	public Usuario getLogado() {
		return logado;
	}

	public void setLogado(Usuario logado) {
		this.logado = logado;
	}

	public void login(Usuario usuario) {
		this.logado = usuario;
	}
	
	public void loginAluno(Aluno logado) {
		this.logaluno = logado;
	}
	
	public void loginProfessor(Professor logado) {
		this.logaprofessor = logado;
	}
	
	public boolean isAtiva() {
		return logado != null;
	}

	public void logout() {
		this.logado = null;
	}

	public Date getDataAtual() {
		return dataAtual;
	}

	public void setDataAtual(Date dataAtual) {
		this.dataAtual = dataAtual;
	}

	public Aluno getLogaluno() {
		return logaluno;
	}

	public void setLogaluno(Aluno logaluno) {
		this.logaluno = logaluno;
	}

	public Professor getLogaprofessor() {
		return logaprofessor;
	}

	public void setLogaprofessor(Professor logaprofessor) {
		this.logaprofessor = logaprofessor;
	}
}