package br.com.sraa.geral.bo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Grupousuario;
import br.com.sraa.geral.model.Permissao;


public class GrupousuarioBO {
	
	private Dao dao;
	
	public GrupousuarioBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Grupousuario procura(Grupousuario dto) {
		 Grupousuario aux = (Grupousuario) dao.procura(dto.getId(), Grupousuario.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Grupousuario armazena (Grupousuario dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Descri��o deve ser informado!");
		 return (Grupousuario)dao.salva(dto);
	}


	 public List<Object> lista(Grupousuario dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Grupousuario.class);

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("descricao",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
		
		return ct.list();
	 }
	 
	 public void remove(Grupousuario dto) throws Exception {
			dao.remove(procura(dto));
		}
	 
	 // observa��o - tirei o .equals na segunda condi��o do if por igual == pois 
	 // o mesmo estava dando warning
	 public Grupousuario armazenaPermissao (Permissao permissao, Grupousuario grupousuario)  throws Exception {
		 if (permissao.getId() == null || permissao.getId()==0)  
				throw new Exception("O campo Permiss�o deve ser informado!");
		 grupousuario = this.procura(grupousuario);
		 grupousuario.getPermissoes().add(permissao);
		 return (Grupousuario)dao.salva(grupousuario);
	}	 
	 
	 
	 public Grupousuario removePermissao (Permissao permissao, Grupousuario grupousuario)  throws Exception {
		 grupousuario = this.procura(grupousuario);
		 permissao = (Permissao)dao.procura(permissao.getId(), Permissao.class);
		 grupousuario.getPermissoes().remove(permissao);
		 return (Grupousuario)dao.salva(grupousuario);
	}	
	
}
