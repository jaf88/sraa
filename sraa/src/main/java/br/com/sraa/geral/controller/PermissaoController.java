package br.com.sraa.geral.controller;


import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.bo.PermissaoBO;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Permissao;


@Controller
public class PermissaoController extends DefaultController  {
	
	private PermissaoBO bo;
	
	public PermissaoController(){}
	
	@Inject
	public PermissaoController(Result result,  Dao dao) {
		super(result,dao);
		bo = new PermissaoBO(dao);
	}
	 
		
	public void listar(Permissao permissao) { 
		 try {
			this.result.include("permissao",permissao);
			lista =  bo.lista(permissao);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	public void salvar(Permissao permissao) {
		try {
			permissao = bo.armazena(permissao);
			this.result.include("permissao",permissao);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(this).listar(new Permissao());
//			this.result.redirectTo(PermissaoController.class).detalhes(permissao);
		} catch (Exception e) {
			this.result.include("permissao",permissao);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(PermissaoController.class).formulario();
		}
	}
	
	public void editar(Permissao permissao) {
		try {
			permissao = bo.procura(permissao);
			result.include("permissao",permissao);
		} catch (Exception e) {
			result.include("permissao",permissao);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Permissao());
		}
	}
	
	public void detalhes(Permissao permissao) {
		try {
			permissao = bo.procura(permissao);
			result.include("permissao",permissao);
		} catch (Exception e) {
			result.include("permissao",permissao);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Permissao());
		}

	}
	
	public void remover(Permissao permissao) {
		try {
			bo.remove(permissao);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(PermissaoController.class).listar(new Permissao());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
}