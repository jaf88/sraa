package br.com.sraa.geral.util;

import java.lang.reflect.InvocationTargetException;

public class JsonDTO {
	
	private Long id;
	
	private String value;
	
	public JsonDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	public JsonDTO(Object obj) {
		try {
			
			this.id = (Long)Reflex.pegaValor(obj,"id");
			this.value = (String)Reflex.pegaValor(obj,"descricao");
			
		} catch (IllegalAccessException | InvocationTargetException e) {
			this.id=0l;
			this.value="";
		}
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	

}
