package br.com.sraa.geral.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.bo.GrupousuarioBO;
import br.com.sraa.geral.bo.UsuarioBO;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Grupousuario;
import br.com.sraa.geral.model.Usuario;


@Controller
public class UsuarioController extends DefaultController {

	private UsuarioBO bo;
	
	public UsuarioController(){}
	
	@Inject
	public UsuarioController(Result result,  Dao dao) {
		super(result,dao);
		bo = new UsuarioBO(dao);
	}
	

	@Override
	public void formulario(){
		try {
			result.include("listaGrupoUsuario",new GrupousuarioBO(dao).lista(new Grupousuario()));
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
	
	public void listar(Usuario usuario) { 
		 try {
			this.result.include("usuario",usuario);
			lista =  bo.lista(usuario);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
	}
	
	public void salvar(Usuario usuario) {
		try {
			usuario = bo.armazena(usuario);
			this.result.include("cliente",usuario);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(this).listar(new Usuario());
		} catch (Exception e) {
			this.result.include("usuario",usuario);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(UsuarioController.class).formulario();
		}
	}
	
	public void editar(Usuario usuario) {
		try {
			usuario = bo.procura(usuario);
			result.include("usuario",usuario);
			result.include("listaGrupoUsuario",new GrupousuarioBO(dao).lista(new Grupousuario()));
		} catch (Exception e) {
			result.include("usuario", usuario);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Usuario());
		}
	}
	
	public void detalhes(Usuario usuario) {
		try {
			usuario = bo.procura(usuario);
			result.include("usuario", usuario);
		} catch (Exception e) {
			result.include("usuario", usuario);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Usuario());
		}

	}
	
	public void remover(Usuario usuario) {
		try {
			bo.remove(usuario);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(UsuarioController.class).listar(new Usuario());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}
}
