package br.com.sraa.geral.bo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Permissao;


public class PermissaoBO {
	
	private Dao dao;
	
	public PermissaoBO(Dao dao) {
		this.dao=dao;
	}
	
	 public Permissao procura(Permissao dto) {
		 Permissao aux = (Permissao) dao.procura(dto.getId(), Permissao.class);
		 return (aux.getId()!=null?aux:null);
	}

	 public Permissao armazena (Permissao dto)  throws Exception {
		 if (dto.getDescricao() == null || dto.getDescricao().equals("")) 
				throw new Exception("O campo Nome deve ser informado!");
		 return (Permissao)dao.salva(dto);
	}


	 public List<Object> lista(Permissao dto)throws Exception {
		Criteria ct = this.dao.getSession().createCriteria(Permissao.class);

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));

		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
		
		return ct.list();
	 }
	 
	 public void remove(Permissao dto) throws Exception {
			dao.remove(procura(dto));
		}
	
}
