package br.com.sraa.geral.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Usuario;


public class BuscaJson {
	private JsonDTO jsondto;
	List listaAux=null;
	ArrayList<JsonDTO> lista= null; 
	private Usuario usuario;
	private final Dao dao;
	
	public BuscaJson(Dao dao) {
		this.dao = dao;
	}
	
	public BuscaJson(Dao daoFactory,Usuario usuario) {
		this.dao = daoFactory;
		this.usuario = usuario; 
	}
	
	/*
	 * Consulta uma entidade por um campo String - input
	 */
	public ArrayList<JsonDTO> busca(String input,String classe) throws Exception{
		Class classeBase =  Class.forName("mor."+classe);
		try {
			MontaLista(input, classeBase);// preenche a vari�vel lista com o resultado da consulta
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	
	public List buscaPorChave(String chave,Long valor,String classe) throws Exception{
		Class classeBase =  Class.forName("mor."+classe);
		try {
			MontaListaPorchave(chave,valor, classeBase);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}
	
		
	private void MontaLista(String input, Class classeBase) throws Exception, IllegalAccessException, InvocationTargetException {
			listaAux = this.listaDados(input,classeBase);
			lista= new ArrayList<JsonDTO>();
			for(Object objeto : listaAux){
				jsondto = new JsonDTO();
				jsondto.setId((Long)Reflex.pegaValor(objeto, "id"));
				jsondto.setValue((String)Reflex.pegaValor(objeto, "descricao"));
	    		lista.add(jsondto);
	    	}
		}
		
	private void MontaListaPorchave(String chave,Long valor, Class classeBase) throws Exception, IllegalAccessException, InvocationTargetException {
			listaAux = this.listaDadosPorChave(chave,valor,classeBase);
			lista= new ArrayList<JsonDTO>();
			for(Object objeto : listaAux){
				jsondto = new JsonDTO();
				jsondto.setId((Long)Reflex.pegaValor(objeto, "id"));
				jsondto.setValue((String)Reflex.pegaValor(objeto, "descricao"));
	    		lista.add(jsondto);
	    	}
		}
		

	 public List<Object> listaDados(String input,Class classe)throws Exception {
			Criteria sa = this.dao.getSession().createCriteria(classe);
			if(input!=null && !input.equals("")){
					sa.add(Restrictions.like("descricao",input,MatchMode.ANYWHERE).ignoreCase());
			}
			sa.add(Restrictions.eq("status",1));
			sa.addOrder(Order.asc("descricao")); 
		 return sa.list();
	}

	 public List<Object> listaDadosPorChave(String chave,Long valor,Class classe)throws Exception {
			Criteria sa = this.dao.getSession().createCriteria(classe);
			if(valor!=null){
				sa.add(Restrictions.eq(chave,valor));
				return sa.list();
			}
			return null;
		
	}
	 

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
