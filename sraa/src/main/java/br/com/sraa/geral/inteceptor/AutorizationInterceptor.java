package br.com.sraa.geral.inteceptor;

/***
 * Copyright (c) 2009 Caelum - www.caelum.com.br/opensource
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.sraa.geral.controller.AdminController;
import br.com.sraa.geral.controller.MainController;
import br.com.sraa.geral.dao.Dao;


/**
 * Interceptor to check if the user is in the session.
 */

@Intercepts
public class AutorizationInterceptor {

	@Inject
	@SessionScoped
	private SessaoInfo sessaoInfo;

	@Inject
	@RequestScoped
	private Dao dao;

	@Inject
	private Result result;
	
	@Inject
	private HttpServletRequest request;

	@Accepts
	public boolean accepts(ControllerMethod method) {
		// Se o metodo
		return !method.containsAnnotation(Public.class);
	}

	/**
	 * Intercepts the request and checks if there is a user logged in.
	 */
	@AroundCall
	public void intercept(SimpleInterceptorStack stack) {
		
		try {
			if(sessaoInfo.getLogado()!=null){
				if(sessaoInfo.getLogado().getGrupousuario().getId()==1) {
					dao.refresh(sessaoInfo.getLogado());
					String url = request.getServletPath().replaceFirst("/", "");
					if(sessaoInfo.getLogado().isAutorizado(url))
						stack.next();
					else // se nao, vai para a pagina de erro de permissao
						result.redirectTo(MainController.class).erroperm();
				}
			}
			else if(sessaoInfo.getLogaprofessor()!=null){
				if(sessaoInfo.getLogaprofessor().getGrupousuario().getId()==2) {
					String url2 = request.getServletPath().replaceFirst("/", "");
					dao.refresh(sessaoInfo.getLogaprofessor());
					// se for autorizado, segue o fluxo
					if(sessaoInfo.getLogaprofessor().isAutorizado(url2))
						stack.next();
					else // se nao, vai para a pagina de erro de permissao
						result.redirectTo(MainController.class).erroperm();
					}
				
			}
			else if(sessaoInfo.getLogaluno()!=null){
				if(sessaoInfo.getLogaluno().getGrupousuario().getId()==3) {
					String url3 = request.getServletPath().replaceFirst("/", "");
					dao.refresh(sessaoInfo.getLogaluno());
					// se for autorizado, segue o fluxo
					if(sessaoInfo.getLogaluno().isAutorizado(url3))
						stack.next();
					else // se nao, vai para a pagina de erro de permissao
						result.redirectTo(MainController.class).erroperm();
					}
				
			}
			
			else{
				result.include("msgErro", "Para sua seguranca, sua sessao foi expirada. "
						+ "Isto ocorre para manter suas informacoes inacessiveis apos um longo periodo de inatividade.");
				result.redirectTo(AdminController.class).index();
			}
			
		} catch (Exception e) {
			result.include("msgErro", "Para sua seguranca, sua sessao foi expirada. EXECAO "
					+ "Isto ocorre para manter suas informacos inacessiveis apos um longo periodo "
					+ "de inatividade."+sessaoInfo.getLogaluno().getMatricula());
			result.redirectTo(AdminController.class).index();
		}

	}
	
}
