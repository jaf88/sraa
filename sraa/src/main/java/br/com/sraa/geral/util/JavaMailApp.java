package br.com.sraa.geral.util;


import java.net.MalformedURLException;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import br.com.sraa.geral.model.Grupousuario;
 
public class JavaMailApp {

	public void enviaEmail(String email, String user, Long idgrupo) throws MalformedURLException {
		    
		Properties props = new Properties();
	    /** Par�metros de conex�o com servidor Gmail */
	    props.put("mail.smtp.host", "smtp.gmail.com");
	    props.put("mail.smtp.socketFactory.port", "465");
	    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.port", "465");
 
	    Session session = Session.getDefaultInstance(props,
	      new javax.mail.Authenticator() {
	           protected PasswordAuthentication getPasswordAuthentication() 
	           {										
	                 return new PasswordAuthentication("sraaifpi@gmail.com", 
	                 "ifpi@informatica2019");
	           }
	      });
 
	    session.setDebug(true);
 
	 try {
		 Message message = new MimeMessage(session);
		 message.setFrom(new InternetAddress("sraaifpi@gmail.com")); 
		 //Remetente
      
		 //Destinat�rio(s)
		 Address[] toUser = InternetAddress 
                 .parse(email);  
 
		 message.setRecipients(Message.RecipientType.TO, toUser);
		 message.setSubject("Cadastro SRAA");//Assunto
		 String host = null;
		 if(idgrupo == 2)
			 host = "\nhttp://localhost:8080/sraaifpictzs/admin/confirmacadastroprofessor";
		 if(idgrupo == 3)
			 host = "\nhttp://localhost:8080/sraaifpictzs/admin/confirmacadastro";
		 
		 String html = "Ol� " + user +" seja bem vindo ao Sistema de Registro de Atividades "
		      		+ "Acad�micas do Instituto Federal do Piau� :D\n"
		      		+ "Clique no link abaixo para confirma seu cadastro\n\n "
		      		+ "Valide seu cadastro em:" + host;
		 message.setText(html);
		 
      Transport.send(message);
 
     } catch (MessagingException e) {
        throw new RuntimeException(e);
    } 
  }
}
