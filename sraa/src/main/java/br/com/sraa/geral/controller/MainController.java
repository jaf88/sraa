package br.com.sraa.geral.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.dao.Dao;

@Controller
public class MainController  {
	
	public MainController(){}
	
	@Inject
	public MainController(Result result,  Dao dao) {}
	
//	public void home() {}
	public void dashboard() {}
	
	public void dashboardadmin() {}
	
	public void dashboardalunoprofessor() {}
	
	public void entenda() {}
	
	public void erropage() {}
		
	public void erro404() {}
	
	public void erroperm() {}
}