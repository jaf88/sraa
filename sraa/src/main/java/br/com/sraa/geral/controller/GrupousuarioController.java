package br.com.sraa.geral.controller;



import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sraa.geral.bo.GrupousuarioBO;
import br.com.sraa.geral.bo.PermissaoBO;
import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Grupousuario;
import br.com.sraa.geral.model.Permissao;


@Controller
public class GrupousuarioController extends DefaultController  {
	
	private GrupousuarioBO bo;
	
	public GrupousuarioController(){}
	
	@Inject
	public GrupousuarioController(Result result,  Dao dao) {
		super(result,dao);
		bo = new GrupousuarioBO(dao);
	}
	 
		
	public void listar(Grupousuario grupousuario) { 
		 try {
			this.result.include("grupousuario",grupousuario);
			lista =  bo.lista(grupousuario);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
		 this.result.include("lista",lista);
		
	}
	
	public void salvar(Grupousuario grupousuario) {
		try {
			grupousuario = bo.armazena(grupousuario);
			this.result.include("grupousuario",grupousuario);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			this.result.redirectTo(GrupousuarioController.class).listar(grupousuario);
		} catch (Exception e) {
			this.result.include("grupousuario",grupousuario);
			this.result.include("msgErro",e.getMessage());
			this.result.redirectTo(GrupousuarioController.class).formulario();
		}
	}
	
	public void editar(Grupousuario grupousuario) {
		try {
			grupousuario = bo.procura(grupousuario);
			result.include("grupousuario",grupousuario);
		} catch (Exception e) {
			result.include("grupousuario",grupousuario);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Grupousuario());
		}
	}
	
	public void detalhes(Grupousuario grupousuario) {
		try {
			grupousuario = bo.procura(grupousuario);
			result.include("grupousuario",grupousuario);
			result.include("listaPermissao",new PermissaoBO(dao).lista(new Permissao()));
		} catch (Exception e) {
			result.include("grupousuario",grupousuario);
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(this).listar(new Grupousuario());
		}

	}
	
	public void remover(Grupousuario grupousuario) {
		try {
			bo.remove(grupousuario);
			this.result.include("msgSucesso","Registro exclu�do com sucesso!");
			result.redirectTo(GrupousuarioController.class).listar(new Grupousuario());
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
		}
	}	
	
	public void salvarPermissao(Permissao permissao, Grupousuario grupousuario) {
		try {
			grupousuario = this.bo.armazenaPermissao(permissao, grupousuario);
//			result.include("permissao",new PermissaoBO(dao).lista(new Permissao()));
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(GrupousuarioController.class).detalhes(grupousuario);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(GrupousuarioController.class).detalhes(grupousuario);
		}
	}
	
	public void removerPermissao(Permissao permissao, Grupousuario grupousuario) {
		try {
			grupousuario = this.bo.removePermissao(permissao, grupousuario);
			this.result.include("msgSucesso","Registro Salvo com sucesso!");
			result.redirectTo(GrupousuarioController.class).detalhes(grupousuario);
		} catch (Exception e) {
			this.result.include("msgErro",e.getMessage());
			result.redirectTo(GrupousuarioController.class).detalhes(grupousuario);
		}
		
	}
}