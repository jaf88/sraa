package br.com.sraa.geral.bo;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.sraa.geral.dao.Dao;
import br.com.sraa.geral.model.Usuario;
import br.com.sraa.geral.util.Reflex;
import br.com.sraa.sistema.model.Aluno;
import br.com.sraa.sistema.model.Professor;



public class UsuarioBO {
	
	private final Dao dao;
	
	public Usuario usuario;

	public UsuarioBO(Dao dao) {
		this.dao = dao;
	}
	
	public UsuarioBO(Dao dao, Usuario usuario) {
		this.dao = dao;
		this.usuario = usuario;
	}
	
	public void remove (Usuario dto) throws Exception {dao.remove(procura(dto));}
	
	public Usuario procura(Usuario dto) { return (Usuario)dao.procura(dto.getId(), Usuario.class); }
	
	public Usuario salva (Usuario dto) throws Exception {
		dto = (Usuario)Reflex.atualiza(dto, dao);
		if(dto.getLogin().equals("") || dto.getSenha().equals(""))
			throw new Exception("O login e senha devem ser digitados");
		if(dto.getSenha().equals(dto.getSenhaConf())) {
			System.out.println("Senha: "+dto.getSenha() +"\n"+"ConfSenha: "+dto.getSenhaConf());
			throw new Exception("As senhas n�o conferem");
		}
		dto.setSenha(dto.getSenha().hashCode()+"");
		
		return this.salva(dto);
	}	

	 public Usuario armazena (Usuario dto)  throws Exception {
		 if(dto.getLogin().equals("") || dto.getSenha().equals(""))
				throw new Exception("O login e senha devem ser digitados");
		 if(!dto.getSenha().equals(dto.getSenhaConf())) {
				System.out.println("Senha: "+dto.getSenha() +"\n"+"ConfSenha: "+dto.getSenhaConf());
				throw new Exception("As senhas n�o conferem");
			}
		 /* if (dto.getDatanasc() == null) 
				throw new Exception("O campo Data de Nascimento deve ser informado!"); */		 
		 dto.setSenha(dto.getSenha().hashCode()+"");
		 // colocar aqui as outras valida�oes
		 return (Usuario)dao.salva(dto);
	}

	public Usuario atualiza (Usuario dto) throws Exception {
		return (Usuario)dao.salva(dto);
	}	
	
//	public List<Object> lista() { return dao.listaTudo(Usuario.class); }
	public List<Object> lista(Usuario dto) {
		Criteria ct = this.dao.getSession().createCriteria(Usuario.class);

		if (dto.getId()!= null) 
			ct.add(Restrictions.eq("id",dto.getId()));
		
		if (dto.getDescricao() != null && !dto.getDescricao().equals("")) 
			ct.add(Restrictions.like("nome",dto.getDescricao(),MatchMode.ANYWHERE).ignoreCase());
		
		return ct.list();
	}	
/*	
	public Set<Modulo> getModulo(Usuario usuario)throws Exception {
		return auxdao.getModulo(usuario);
	}
*/
// Fun��o de Autenica��o normal - Funcionando
//	public Usuario autentica (Usuario dto) throws Exception {
//		Usuario auxdto=null;
//		try{
//			Criteria sa = this.dao.getSession().createCriteria(Usuario.class);
//			if(dto.getLogin()!=null && dto.getSenha()!=null){
//					sa.add(Restrictions.eq("login",dto.getLogin()));
//					sa.add(Restrictions.eq("status",1));
//					List<Usuario> list = (List<Usuario>)sa.list();
//					if(list.size()>0){
//						auxdto =(Usuario)list.get(0);
//						//cria o codigo hash da senha digitada digitada no formul�rio
//						String senhaDigitada = dto.getSenha().hashCode()+""; 
//						
//						// compara com a senha do banco, retorna o usuario n�o forem iguais e null caso contr�rio
//						return (senhaDigitada.equals(auxdto.getSenha()) ? auxdto : null);
//					}	
//			}
//		}catch (Exception e) {
//			throw new Exception("Erro ao efetuar login: "+ e.getMessage());
//		}
//		return auxdto;
//	}
	//Teste de uma Fun��o de Autentica��o Generalizada para qualquer usuario
	public Usuario autentica (Usuario dto) throws Exception {
		Usuario auxdto=null;
		Criteria sa;
		try{
			
			sa = this.dao.getSession().createCriteria(Usuario.class);
			
			if(dto.getLogin()!=null && dto.getSenha()!=null){
					sa.add(Restrictions.eq("login",dto.getLogin()));
					sa.add(Restrictions.eq("status",1));
					List<Usuario> list = (List<Usuario>)sa.list();
					if(list.size()>0){
						auxdto =(Usuario)list.get(0);
						//cria o codigo hash da senha digitada digitada no formul�rio
						String senhaDigitada = dto.getSenha().hashCode()+""; 
						
						// compara com a senha do banco, retorna o usuario n�o forem iguais e null caso contr�rio
						return (senhaDigitada.equals(auxdto.getSenha()) ? auxdto : null);
					}	
			}
		}catch (Exception e) {
			throw new Exception("Erro ao efetuar login: "+ e.getMessage());
		}
		return auxdto;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
		
		
/*	public void executaComando (String sql) throws Exception {
		try{
			if(!usuario.isAdm())
				throw new Exception("Somente Super-usu�rios podem realizar essa opera��o.");
			dao.executaComando(sql);
		}catch (Exception e) {
			throw new Exception("Erro ao executar Comando: "+ e.getMessage());
		}
	}*/
	
}


