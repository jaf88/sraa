package br.com.sraa.geral.inteceptor;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import br.com.caelum.vraptor.AfterCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.sraa.geral.dao.Dao;



@Intercepts
@RequestScoped
public class DaoInterceptor {

	private final Dao dao;
	
	public DaoInterceptor() {
		this.dao=null;
	}
	
	@Inject
	public DaoInterceptor(Dao dao) {
		this.dao=dao;
	}
	
	@AfterCall
	public void after() {
		if (dao.hasTransaction()) {// se sobrou transacao sem comitar, faz rollback
			dao.rollback();
		}
	}	
    
}